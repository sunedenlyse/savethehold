﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Build.Reporting;

public class IdInstantiator : MonoBehaviour
{
    [MenuItem("Ids/Instantiate Ids")]
    public static void MyBuild()
    {
        int charIdsSet   = SetAllCharacterIds();
        int sgIdsSet     = SetAllSpriteGroupIds();
        int statusIdsSet = StatusManager.SetAllStatusIds();
        int cardIdsSet = SetAllCardIds();
        int locationIdsSet = SetAllLocationIds();
        Debug.Log("Instantiated all ids: \n" +
            "  character ids set:    " + charIdsSet+ "\n" +
            "  spritegroups ids set: " + sgIdsSet + "\n" +
            "  status ids set:       " + statusIdsSet + "\n" +
            "  cards ids set :       " + cardIdsSet + "\n" +
            "  location ids set :    " + locationIdsSet
        );

        // I'm not sure the below code works
        /*
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] { "Assets/Main Scene.unity" };
        buildPlayerOptions.locationPathName = "builds/WindowsBuild";
        buildPlayerOptions.target = BuildTarget.StandaloneWindows64;
        buildPlayerOptions.options = BuildOptions.None;
        BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
        BuildSummary summary = report.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
        }

        if (summary.result == BuildResult.Failed)
        {
            Debug.Log("Build failed");
        }
        */
    }

    public static int SetAllCharacterIds()
    {
        List<Character> characters = CharacterManager.GetCharacters();
        for (int i = 0; i < characters.Count; i++)
        {
            characters[i].id = i;
        }
        return characters.Count;
    }

    public static int SetAllSpriteGroupIds()
    {
        List<SpriteGroup> spriteGroups = CharacterManager.GetSpriteGroups();
        for (int i = 0; i < spriteGroups.Count; i++)
        {
            spriteGroups[i].id = i;
        }
        return spriteGroups.Count;
    }

    private static int SetAllCardIds()
    {
        UnityEngine.Object[] allCards = Resources.LoadAll("", typeof(Card));
        for (int i = 0; i < allCards.Length; i++)
        {
            Card card = allCards[i] as Card;
            card.dialogue.cardId = i;
            Debug.Log("Setting dialogue " + card.name + " cardId to " + card.dialogue.cardId);
        }
        return allCards.Length;
    }

    private static int SetAllLocationIds()
    {
        UnityEngine.Object[] allLocations = Resources.LoadAll("", typeof(Location));
        for (int i = 0; i < allLocations.Length; i++)
        {
            Location location = allLocations[i] as Location;
            location.id = i;
        }
        return allLocations.Length;
    }
}
