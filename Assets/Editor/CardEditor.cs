﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Card))]
[CanEditMultipleObjects]
public class CardEditor : AbstractCheckConsequenceEditor
{
    public override void OnInspectorGUI()
    {
        bool foundInconsistency = false;
        Card card = (Card)target;
        if(card.dialogue != null && card.dialogue.options != null)
        {
            foreach (DialogueOption dOption in card.dialogue.options)
            {
                foreach(DialogueConsequence dConsequence in dOption.consequences)
                {
                    foundInconsistency = foundInconsistency || AddInConsistencyLabels(dConsequence);
                }
            }
            if (!foundInconsistency)
            {
                EditorGUILayout.LabelField("Addable consistency:", "OK");
            }
        }
        // Show default inspector property editor
        DrawDefaultInspector();
    }
}
