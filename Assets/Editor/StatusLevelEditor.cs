﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(StatusLevel))]
[CanEditMultipleObjects]
public class StatusLevelEditor : AbstractCheckConsequenceEditor
{
    public override void OnInspectorGUI()
    {
        bool foundInconsistency = false;
        StatusLevel statusLevel = (StatusLevel)target;
        if(statusLevel.consequenceThresholds == null)
        {
            statusLevel.consequenceThresholds = new List<ConsequenceThreshold>();
        }
        foreach (ConsequenceThreshold cThreshold in statusLevel.consequenceThresholds)
        {
            DialogueConsequence dConsequence = cThreshold.consequence;
            foundInconsistency = foundInconsistency || AddInConsistencyLabels(dConsequence);
        }
        if (!foundInconsistency)
        {
            EditorGUILayout.LabelField("Addable consistency:", "OK");
        }
        // Show default inspector property editor
        DrawDefaultInspector();
    }
}
