﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(DialogueConsequence))]
[CanEditMultipleObjects]
public class ConsequenceDrawer : PropertyDrawer {

    int enumValueChosen = 0;

    private List<ConsequenceType> typesShowingInt = new List<ConsequenceType> {
        ConsequenceType.ChangeStatus,
        ConsequenceType.ChangeStatusDelta,
        ConsequenceType.SetStatusDelta,
        ConsequenceType.WaitUntilStatusReaches};

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        var amountRect = new Rect(position.x, position.y,75, position.height);
        var unitRect = new Rect(position.x + 80, position.y, 175, position.height);
        var nameRect = new Rect(position.x + 260, position.y, position.width - 260, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        SerializedProperty consequenceProp = property.FindPropertyRelative("consequenceType");
        ConsequenceType type = (ConsequenceType) consequenceProp.enumValueIndex;
        EditorGUI.PropertyField(amountRect, consequenceProp, GUIContent.none);
        EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("addable"), GUIContent.none);
        if(typesShowingInt.Contains(type))
        {
            EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("valueInt"), GUIContent.none);
        }
        if (ConsequenceType.ChangeDynamicText == type)
        {
            EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("valueString"), GUIContent.none);
        }
        if (ConsequenceType.ChangeSprite== type)
        {
            EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("valueSprite"), GUIContent.none);
        }

        if (ConsequenceType.ChangeReplaceActiveCard == type)
        {
            EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("valueLocation"), GUIContent.none);
        }
        if (ConsequenceType.SetStatus == type)
        {
            Object addable = property.FindPropertyRelative("addable").objectReferenceValue;
            if (addable is StatusLevel && ((StatusLevel) addable).HasPossibleValuesList())
            {
                if(property.FindPropertyRelative("valueInt") != null)
                {
                    enumValueChosen = property.FindPropertyRelative("valueInt").intValue;
                    string[] enumValueChoices = ((StatusLevel) addable).GetPossibleValues().ToArray();
                    enumValueChosen = EditorGUI.Popup(nameRect, enumValueChosen, enumValueChoices);
                    // Update the selected choice in the underlying object
                    property.FindPropertyRelative("valueInt").intValue = enumValueChosen;
                }
            }
            else
            {
                EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("valueInt"), GUIContent.none);
            }
        }

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
