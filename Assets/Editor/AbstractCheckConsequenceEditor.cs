﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public abstract class AbstractCheckConsequenceEditor : Editor {

    protected static bool AddInConsistencyLabels(DialogueConsequence dConsequence)
    {
        List<string> errorMessages = new List<string>();
        bool foundInconsistency = false;
        switch (dConsequence.consequenceType)
        {
            case ConsequenceType.RemoveCard:
                foundInconsistency = CheckForInconsistency<Card>(foundInconsistency, errorMessages, dConsequence, false);
                break;
            case ConsequenceType.AddCard:
                foundInconsistency = CheckForInconsistency<Card>(foundInconsistency, errorMessages, dConsequence, false);
                break;
            case ConsequenceType.ImmediateDialogue:
                foundInconsistency = CheckForInconsistency<Card>(foundInconsistency, errorMessages, dConsequence, false);
                break;
            case ConsequenceType.TriggerAchievement:
                foundInconsistency = CheckForInconsistency<Achievement>(foundInconsistency, errorMessages, dConsequence, false);
                break;
            case ConsequenceType.EndGame:
                foundInconsistency = CheckForInconsistency<MusicGroup>(foundInconsistency, errorMessages, dConsequence, true);
                break;
            case ConsequenceType.ChangeStatus:
                foundInconsistency = CheckForInconsistency<StatusLevel>(foundInconsistency, errorMessages, dConsequence, false);
                break;
            case ConsequenceType.SetStatus:
                foundInconsistency = CheckForInconsistency<StatusLevel>(foundInconsistency, errorMessages, dConsequence, false)
                    && CheckForInconsistency<EnumStatusLevel>(foundInconsistency, errorMessages, dConsequence, false)
                    && CheckForInconsistency<BoolStatusLevel>(foundInconsistency, errorMessages, dConsequence, false);
                break;
            case ConsequenceType.ChangeStatusDelta:
                foundInconsistency = CheckForInconsistency<StatusLevel>(foundInconsistency, errorMessages, dConsequence, false);
                break;
            case ConsequenceType.SetStatusDelta:
                foundInconsistency = CheckForInconsistency<StatusLevel>(foundInconsistency, errorMessages, dConsequence, false);
                break;
            case ConsequenceType.ChangeDynamicText:
                foundInconsistency = CheckForInconsistency<DynamicString>(foundInconsistency, errorMessages, dConsequence, false);
                break;
            case ConsequenceType.ChangeSprite:
                foundInconsistency = CheckForInconsistency<Character>(foundInconsistency, errorMessages, dConsequence, false);
                break;
            case ConsequenceType.WaitUntilStatusReaches:
                foundInconsistency = CheckForInconsistency<StatusLevel>(foundInconsistency, errorMessages, dConsequence, false);
                break;
            case ConsequenceType.ChangeReplaceActiveCard:
                foundInconsistency = CheckForInconsistency<Card>(foundInconsistency, errorMessages, dConsequence, false);
                break;
            default:
                Debug.LogError("ERROR: consequence of type: " + dConsequence.consequenceType + " not handled when checking for inconsistencies.");
                break;
        }
        if(foundInconsistency)
        {
            foreach (string errorMessage in errorMessages)
            {
                EditorGUILayout.LabelField("Addable inconsistency found:", errorMessage);
            }
        }
        return foundInconsistency;
    }

    private static bool CheckForInconsistency<T>(bool foundInconsistency, List<string> errorMessages, DialogueConsequence dConsequence, bool nullAllowed) where T : ConsequenceAddable
    {
        if (dConsequence.addable == null)
        {
            if (!nullAllowed)
            {
                //EditorGUILayout.LabelField("Addable inconsistency found:", "Type " + dConsequence.consequenceType + " expects addable " + typeof(T).Name + ", found null");
                errorMessages.Add("Type " + dConsequence.consequenceType + " expects addable " + typeof(T).Name + ", found null");
                foundInconsistency = true;
            }
        }
        else if (!(dConsequence.addable.GetType() == typeof(T)))
        {
            //EditorGUILayout.LabelField("Addable inconsistency found:", "Type " + dConsequence.consequenceType + " expects addable " + typeof(T).Name + ", found " + dConsequence.addable.GetType());
            errorMessages.Add("Type " + dConsequence.consequenceType + " expects addable " + typeof(T).Name + ", found " + dConsequence.addable.GetType());
            foundInconsistency = true;
        }

        return foundInconsistency;
    }
}
