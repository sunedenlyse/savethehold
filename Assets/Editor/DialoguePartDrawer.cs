﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(DialoguePart))]
public class DialoguePartDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        Rect propertyRect = new Rect(position.x, position.y, position.width, 200);
        EditorGUI.BeginProperty(propertyRect, label, property);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        var characterRect = new Rect(position.x, position.y, 100, 15);
        var spriteOptionRect = new Rect(position.x + 101, position.y, 10, 15);
        var spriteIndexRect = new Rect(position.x + 111, position.y, 90, 15);
        var backgroundRect = new Rect(position.x + 202, position.y, position.width - 202, 15);
        Rect textAreaRect = new Rect(position.x, position.y, position.width, 100);
        SerializedProperty characterProp = property.FindPropertyRelative("character");
        Character character = (Character)characterProp.objectReferenceValue;
        if(character != null && character.expectedSprites != null && character.expectedSprites.Count > 0)
        {
            int spriteChosen = property.FindPropertyRelative("spriteIndex").intValue;
            string[] spriteChoices = character.expectedSprites.ToArray();
            spriteChosen = EditorGUI.Popup(spriteIndexRect, spriteChosen, spriteChoices);
            // Update the selected choice in the underlying object
            property.FindPropertyRelative("spriteIndex").intValue = spriteChosen;
        }
        EditorGUI.PropertyField(characterRect, characterProp, GUIContent.none);
        EditorGUI.PropertyField(spriteOptionRect, property.FindPropertyRelative("noSprite"), GUIContent.none);
        EditorGUI.PropertyField(backgroundRect, property.FindPropertyRelative("background"), GUIContent.none);
        EditorGUI.PropertyField(textAreaRect, property.FindPropertyRelative("sentence"), GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 100f;
    }
}
