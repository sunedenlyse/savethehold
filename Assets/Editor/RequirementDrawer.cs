﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(DialogueOptionRequirement))]
[CanEditMultipleObjects]
public class RequirementDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        var amountRect = new Rect(position.x, position.y, 150, position.height);
        var unitRect = new Rect(position.x + 155, position.y, 100, position.height);
        var nameRect = new Rect(position.x + 260, position.y, position.width - 260, position.height);
        var EnumValueRect = new Rect(position.x + 155, position.y, position.width - 155, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        SerializedProperty statusLevelProp = property.FindPropertyRelative("statusLevel");
        EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("statusLevel"), GUIContent.none);
        if (statusLevelProp.objectReferenceValue != null && ((StatusLevel)statusLevelProp.objectReferenceValue).HasPossibleValuesList())
        {
            int enumValueChosen = property.FindPropertyRelative("value").intValue;
            string[] enumValueChoices = ((StatusLevel)statusLevelProp.objectReferenceValue).GetPossibleValues().ToArray();
            enumValueChosen = EditorGUI.Popup(EnumValueRect, enumValueChosen, enumValueChoices);
            // Update the selected choice in the underlying object
            property.FindPropertyRelative("value").intValue = enumValueChosen;

        }
        else if(((StatusLevel)statusLevelProp.objectReferenceValue) is StatusLevel)
        {
            EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("orderType"), GUIContent.none);
            EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("value"), GUIContent.none);
        }

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
