﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Linq;
using System.IO;
using UnityEditor;

public class MapNavigator : MonoBehaviour {
    #region singleton
    public static MapNavigator instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            throw new System.Exception("Found multiple instances of " + this.name);
        }
    }
    #endregion
    public Renderer textureRenderer;
    public Camera gameCamera;
    public Transform player;
    public int playerOffset;
    public Transform explorationIcon;
    public DialogueTrigger dialogueTrigger; // Why does the MapNavigator have the DialogueTrigger? 
    private Location startLocation;
    public Card activeEventCard;
    public DynamicString locationDynamicString;
    public Transform locations;
    private Location currentLocation;

    private bool explorationIconEnabled;
    
    public void UpdateMapAndLocationsToScenario(Scenario scenario)
    {
        DrawTexture(scenario.mapTexture);
        DestroyCurrentLocations();
        InstantiateLocations(scenario);
        Material playerMaterial = player.gameObject.GetComponent<MeshRenderer>().material;
        if(scenario.playerIcon!= null)
        {
            player.gameObject.SetActive(true);
            playerMaterial.mainTexture = scenario.playerIcon.texture;
        }
        else
        {
            player.gameObject.SetActive(false);
        }
        Material explorationIconMaterial = explorationIcon.gameObject.GetComponent<MeshRenderer>().material;
        if (scenario.explorationIcon != null)
        {
            //Debug.Log("Enabling ExplorationIcon");
            explorationIconMaterial.mainTexture = scenario.explorationIcon.texture;
            explorationIconEnabled = true;
        }
        else
        {
            //Debug.Log("Disabling ExplorationIcon");
            explorationIconEnabled = false;
        }
    }

    public void InstantiateLocations(Scenario scenario)
    {
        //Debug.Log("Instantiating locations for scenario " + scenario.name + " and setting starting location to " + scenario.startLocation.name);
        List<Location> loadedLocations = Resources.LoadAll(scenario.name, typeof(Location)).Cast<Location>().ToList();
        foreach (Location location in loadedLocations)
        {
            Location newLocation = GameObject.Instantiate(location, locations);
            newLocation.name = location.name;
            
            //Debug.Log("Instantiating location " + newLocation.name);
            if (newLocation.id == scenario.startLocation.id)
            {
                startLocation = newLocation;
                //Debug.Log("Setting Startlocation to " + startLocation.name);
            }
        }
    }

    public void ResetAllReplacesActiveCardOnLocation()
    {
        List<Location> locationPrefabs = Resources.LoadAll(ScenarioManager.instance.GetCurrentScenarioName(), typeof(Location)).Cast<Location>().ToList();
        Location[] locationInstances = locations.GetComponentsInChildren<Location>();
        foreach (Location locationInstance in locationInstances)
        {
            foreach (Location locationPrefab in locationPrefabs)
            {
                if (locationInstance.id == locationPrefab.id && locationPrefab.replacesActiveCard != null)
                {
                    //Debug.Log("Setting " + locationPrefab.replacesActiveCard.name + " as new active card on " + locationInstance.name);
                    locationInstance.replacesActiveCard = locationPrefab.replacesActiveCard;
                }
            }
        }
    }

    public void SetReplacesActiveCardOnLocation(int locationId, Card card)
    {
        Location[] locationInstances = locations.GetComponentsInChildren<Location>();
        foreach (Location locationInstance in locationInstances)
        {
            if(locationInstance.id == locationId)
            {
                Debug.Log("Setting " + card.name + " as new active card on " + locationInstance.name);
                locationInstance.replacesActiveCard = card;
            }
        }
    }

    public void DestroyCurrentLocations()
    {
        Location[] locationsFound= locations.GetComponentsInChildren<Location>();
        foreach(Location location in locationsFound)
        {
            GameObject.DestroyImmediate(location.gameObject);
        }
    }
    


        public void ResetPlayerPosition()
    {
        //Debug.Log("Moving player to StartLocation");
        MovePlayerToLocation(startLocation);
    }
    

    private void Update()
    {
        bool isCurrentLocation = false;
        Location location = CheckClickedLocation(out isCurrentLocation);
        if(location != null)
        {
            if(isCurrentLocation)
            {
                if(location.replacesActiveCard == null)
                {
                    DisplayActiveEventDialogue(location);
                }
                else
                {
                    //Debug.Log("Triggering active event dialogue on " + location.name + ": " + location.replacesActiveCard.name);
                    dialogueTrigger.dialogue = location.replacesActiveCard.dialogue;
                    dialogueTrigger.TriggerDialogue();
                    CardLibrary.instance.RemoveSingleCardFromDeck(location.replacesActiveCard);
                }
            } else
            {
                MovePlayerToLocation(location);
                TriggerRandomEvent(location);
                StatusManager.instance.ChangeAllStatusesAtMove();
            }
        }
    }

    private void DrawTexture(Texture2D texture)
    {
        textureRenderer.sharedMaterial.mainTexture = texture;
        //textureRenderer.transform.localScale = new Vector3(texture.width, 1, texture.height) / 10f;
    }

    private void TriggerRandomEvent(Location location)
    {
        Card card = CardLibrary.instance.DrawRandomCardFromDeck(location);
        dialogueTrigger.dialogue = card.dialogue;
        dialogueTrigger.TriggerDialogue();
    }

    private void DisplayActiveEventDialogue(Location location)
    {
        Dialogue dialogueToDisplay = ResetActiveEventDialogue();
        List<Card> activeCards = CardLibrary.instance.GetActiveCards(location);
        foreach(Card card in activeCards)
        {
            DialogueOption dialogueOption = new DialogueOption
            {
                buttonText = card.description
            };
            DialogueConsequence consequence = new DialogueConsequence
            {
                consequenceType = ConsequenceType.ImmediateDialogue,
                addable = card
            };
            dialogueOption.consequences = new List<DialogueConsequence>
            {
                consequence
            };
            dialogueOption.requirements = new List<DialogueOptionRequirement>();
            dialogueToDisplay.options.Add(dialogueOption);
        }
        dialogueTrigger.dialogue = dialogueToDisplay;
        dialogueTrigger.TriggerDialogue();
    }

    private Dialogue ResetActiveEventDialogue()
    {
        Dialogue dialogueToDisplay = new Dialogue
        {
            parts = activeEventCard.dialogue.parts,
            options = new List<DialogueOption>()
        };
        foreach (DialogueOption option in activeEventCard.dialogue.options)
        {
            dialogueToDisplay.options.Add(option);
        }
        return dialogueToDisplay;
    }

    public void MovePlayerToLocation(Location location)
    {
        Vector3 newPlayerPosition = location.transform.position + playerOffset * Vector3.forward;
        newPlayerPosition.y = 1;

        player.position = newPlayerPosition;
        currentLocation = location;
        DynamicStringManager.instance.ChangeDynamicString(locationDynamicString, location.name);
        UpdateExplorationIcon();
    }

    private void UpdateExplorationIcon()
    {
        if (explorationIconEnabled && CardLibrary.instance.GetActiveCards(currentLocation).Count > 0)
        {
            Vector3 newExplorationPosition = currentLocation.transform.position + playerOffset * Vector3.back;
            newExplorationPosition.y = 1;
            explorationIcon.position = newExplorationPosition;
            explorationIcon.gameObject.SetActive(true);
        } else
        {
            explorationIcon.gameObject.SetActive(false);
        }
    }

    private Location CheckClickedLocation(out bool isCurrentLocation)
    {
        isCurrentLocation = false;
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return null;
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = gameCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo))
            {
                //Debug.Log("You clicked " + hitInfo.transform.gameObject.name); ;
                Location location = hitInfo.transform.gameObject.GetComponent<Location>();
                if (location != null)
                {
                    //Debug.Log("You clicked " + hitInfo.transform.gameObject.name); ;
                    if (LocationIsNeighbourToCurrent(location))
                    {
                        isCurrentLocation = false;
                        return location;
                    }
                    if (LocationIsCurrent(location))
                    {
                        isCurrentLocation = true;
                        return location;
                    }
                }
            }
        }
        return null;
    }

    public Location GetCurrentLocation()
    {
        return currentLocation;
    }

    public Location GetLocationById(int id)
    {
        Location[] locationsFound = locations.GetComponentsInChildren<Location>();
        foreach (Location location in locationsFound)
        {
            if (location.id == id)
            {
                return location;
            }
        }
        return null;
    }

    public bool LocationIsCurrent(Location location)
    {
        //Debug.Log("location " + location.name + " is current?: " + location.Equals(currentLocation));
        return location.Equals(currentLocation);
    }

    public bool LocationIsNeighbourToCurrent(Location location)
    {
        for (int i = 0; i < currentLocation.neighbours.Count; i++)
        {
            //Debug.Log("Current Location " + currentLocation.name + " has " + currentLocation.neighbours.Count + " neighbours");
            if(location.name.Equals(currentLocation.neighbours[i].name))
            {

                return true;
            }
        }
        return false;
    }
}
