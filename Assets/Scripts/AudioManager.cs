﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour {
    
    public static AudioManager instance;
    
    public AudioSource musicAudioSource;
    public AudioSource dialogueAudioSource;
    public float musicDelay;
    public static readonly string muteMusicKey = "options_mute_music";
    public static readonly string muteDialogueKey = "options_mute_dialogue";
    private MusicGroup currentlyPlayingMusicGroup;
    private MusicGroup defaultMusicGroup;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            throw new System.Exception("Found multiple instances of " + this.name);
        }
    }

    public void SetDefaultMusicGroup(MusicGroup musicGroup)
    {
        defaultMusicGroup = musicGroup;
    }

    public void SetMuteDialogue(Toggle toggle)
    {
        bool muteDialogue = !toggle.isOn;
        BoolPlayerPrefs.SetBool(muteDialogueKey, muteDialogue);
    }

    public void SetMuteMusic(Toggle toggle)
    {
        bool muteMusic = !toggle.isOn;
        BoolPlayerPrefs.SetBool(muteMusicKey, muteMusic);
        if (muteMusic)
        {
            //Debug.Log("Stopping music");
            musicAudioSource.Stop();
            StopCoroutine(PlayRandomMusic());
        }
        else
        {
            //Debug.Log("Starting music");
            UIManager.instance.PlayMainMenuTheme();
        }
    }

    public void PlayMusic(MusicGroup musicGroup)
    {
        if (!BoolPlayerPrefs.GetBool(muteMusicKey))
        {
            if(musicGroup != null)
            {
                //Debug.Log("Playing music " + musicGroup.name);
                StartPlayingRandomMusic(musicGroup);
            }
            else if(defaultMusicGroup != null)
            {
                //Debug.Log("Playing default music");
                StartPlayingRandomMusic(defaultMusicGroup);
            }
        }
    }

    private void StartPlayingRandomMusic(MusicGroup musicGroup)
    {
        if(currentlyPlayingMusicGroup == null || !currentlyPlayingMusicGroup.Equals(musicGroup))
        {
            musicAudioSource.Stop();
            StopCoroutine(PlayRandomMusic());
            currentlyPlayingMusicGroup = musicGroup;
            StartCoroutine(PlayRandomMusic());
        }
    }

    private IEnumerator PlayRandomMusic()
    {
        while (true)
        {
            //Debug.Log("Checking for new music");
            if (!musicAudioSource.isPlaying && currentlyPlayingMusicGroup != null && !BoolPlayerPrefs.GetBool(muteMusicKey))
            {
                musicAudioSource.clip = GetRandomClip(currentlyPlayingMusicGroup);
                musicAudioSource.Play();
            }
            yield return new WaitForSeconds(musicDelay);
        }
    }

    private AudioClip GetRandomClip(MusicGroup musicGroup)
    {
        //Debug.Log("Getting random clip from music " + musicGroup.name + " to play");
        int randomIndex = Random.Range(0, musicGroup.audioClips.Count - 1);
        return musicGroup.audioClips[randomIndex];
    }

    public void PlayDialogueClip(AudioClip audioClip)
    {
        if(!BoolPlayerPrefs.GetBool(muteDialogueKey))
        {
            //Debug.Log("Play dialogue clip " + audioClip.name);
            dialogueAudioSource.Stop();
            dialogueAudioSource.PlayOneShot(audioClip);
        }
    }

}
