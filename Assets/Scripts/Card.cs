﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Card", menuName = "Card")]
public class Card : ConsequenceAddable
{
    public string description;
    public List<Zone> allowedZones;
    public List<Location> allowedLocations;
    public CardPriority priority;
    public CardType cardType;
    public Dialogue dialogue;
}

public enum CardPriority {normal, quest }
public enum CardType { Random, Active, randomAndActive}
