﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventManager : MonoBehaviour {
    #region singleton
    public static EventManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            throw new System.Exception("Found multiple instances of EventManager");
        }
    }
    #endregion
    
    public void HandleConsequence(DialogueConsequence consequence, Button optionResultButton)
    {
        //Debug.Log("Handling consequence of type " + consequence.consequenceType);
        switch (consequence.consequenceType)
        {
            case ConsequenceType.RemoveCard:
                if(consequence.addable is Card)
                {
                        CardLibrary.instance.RemoveSingleCardFromDeck((Card) consequence.addable);
                } else
                {
                    Debug.LogWarning("consequence of type RemoveCard did not have a card attached");
                }
                break;
            case ConsequenceType.AddCard:
                if (consequence.addable is Card)
                {
                    CardLibrary.instance.AddCardToDeck((Card)consequence.addable);
                }
                else
                {
                    Debug.LogWarning("consequence of type AddCard did not have a card attached");
                }
                break;
            case ConsequenceType.ImmediateDialogue:
                if (consequence.addable is Card)
                {
                    //Debug.Log("putting dialogue on queue from card " + ((Card)consequence.addable).name);
                    StHDialogueManager.instance.PutDialogueOnQueue(((Card)consequence.addable).dialogue);
                }
                else
                {
                    Debug.LogWarning("consequence of type ImmediateDialogue did not have a card attached");
                }
                break;
            case ConsequenceType.TriggerAchievement:
                if (consequence.addable is Achievement)
                {
                    AchievementManager.instance.TriggerAchievement((Achievement)consequence.addable);
                    Debug.Log("Trigger achievement: " + consequence.addable.name);
                }
                else
                {
                    Debug.LogWarning("consequence of type TriggerAchievement did not have a Achievement attached");
                }
                break;
            case ConsequenceType.EndGame:
                MusicGroup endMusic = null;
                if(consequence.addable is MusicGroup)
                {
                    endMusic = (MusicGroup) consequence.addable;
                }
                if(optionResultButton != null){
                    optionResultButton.onClick.AddListener(() => ExitManager.instance.GameEndPopUp(endMusic));
                } else
                {
                    ExitManager.instance.GameEndPopUp(endMusic);
                }
                break;
            case ConsequenceType.ChangeStatus:
                if(consequence.addable is StatusLevel)
                {
                    StatusLevel statusLevel = (StatusLevel)consequence.addable;
                    statusLevel.ChangeBy(consequence.valueInt);
                } else
                {
                    Debug.LogWarning("consequence of type ChangeStatus did not have a StatusLevel attached");
                }
                break;
            case ConsequenceType.SetStatus:
                if (consequence.addable is StatusLevel)
                {
                    StatusLevel statusLevel = (StatusLevel)consequence.addable;
                    statusLevel.SetTo(consequence.valueInt, false);
                }
                else
                {
                    Debug.LogWarning("consequence of type ChangeStatus did not have a StatusLevel attached");
                }
                break;
            case ConsequenceType.ChangeStatusDelta:
                if (consequence.addable is StatusLevel)
                {
                    StatusLevel statusLevel = (StatusLevel)consequence.addable;
                    statusLevel.ChangeDeltaBy(consequence.valueInt);
                }
                else
                {
                    Debug.LogWarning("consequence of type ChangeStatus did not have a StatusLevel attached");
                }
                break;
            case ConsequenceType.SetStatusDelta:
                if (consequence.addable is StatusLevel)
                {
                    StatusLevel statusLevel = (StatusLevel)consequence.addable;
                    statusLevel.SetDeltaTo(consequence.valueInt);
                }
                else
                {
                    Debug.LogWarning("consequence of type ChangeStatus did not have a StatusLevel attached");
                }
                break;
            case ConsequenceType.ChangeDynamicText:
                if(consequence.addable is DynamicString)
                {
                    DynamicString addable = (DynamicString)consequence.addable;
                    DynamicStringManager.instance.ChangeDynamicString(addable, consequence.valueString);
                }
                else
                {
                    Debug.LogWarning("consequence of type ChangeDynamicText did not have a DynamicString attached");
                }
                break;
            case ConsequenceType.ChangeSprite:
                if (consequence.addable is Character)
                {
                    Character addable = (Character)consequence.addable;
                    addable.SetSpriteGroup(consequence.valueSprite);
                }
                else
                {
                    Debug.LogWarning("consequence of type ChangeSprite did not have a Character attached");
                }
                break;
            case ConsequenceType.WaitUntilStatusReaches:
                if (consequence.addable is StatusLevel)
                {
                    StatusLevel statusLevel = (StatusLevel)consequence.addable;
                    ((StHDialogueManager)StHDialogueManager.instance).WaitUntilStatusLevelReachesThreshold(statusLevel, consequence.valueInt);
                }
                else
                {
                    Debug.LogWarning("consequence of type WaitUntilStatusReaches did not have a StatusLevel attached");
                }
                break;
            case ConsequenceType.ChangeReplaceActiveCard:
                if (consequence.addable is Card)
                {
                    Card newActiveCard = (Card)consequence.addable;
                    MapNavigator.instance.SetReplacesActiveCardOnLocation(consequence.valueLocation.id, newActiveCard);
                }
                else
                {
                    Debug.LogWarning("consequence of type ChangeReplaceActiveCard did not have a Card attached");
                }
                break;
            default:
                Debug.LogWarning("Consequence of type " + consequence.consequenceType + " not handled!");
                break;
        }
    }

}
