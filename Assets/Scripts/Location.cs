﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Location : MonoBehaviour{

    public List<Location> neighbours;
    public Card replacesActiveCard;
    public int id;

    void OnMouseEnter()
    {
        if(!EventSystem.current.IsPointerOverGameObject() && (MapNavigator.instance.LocationIsNeighbourToCurrent(this) || MapNavigator.instance.LocationIsCurrent(this)))
        {
            //Debug.Log("mouse entered " + this.name);
            UIManager.instance.ChangeCursorToClick();
        }
    }

    void OnMouseExit()
    {
        //Debug.Log("mouse exited " + this.name);
        UIManager.instance.ChangeCursorToDefault();
    }
}
