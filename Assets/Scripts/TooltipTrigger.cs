﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TooltipTrigger : MonoBehaviour {

    public string toolTipMessage;
    public Text tooltipText;

    public void SetToolTip()
    {
        tooltipText.text = toolTipMessage;
    }

    public void ResetToolTip()
    {
        tooltipText.text = "";
    }
}
