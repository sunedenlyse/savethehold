﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
public class AchievementManager : MonoBehaviour {

    #region singleton
public static AchievementManager instance;

private void Awake()
{
    if (instance == null)
    {
        instance = this;
    }
    else
    {
        throw new System.Exception("Found multiple instances of AchievementManager");
    }
}
    #endregion

    //private List<Achievement> achievements;
    private Dictionary<string, List<Achievement>> achievementsd;
    public Sprite uncompletedAchievementIcon;
    public Sprite uncompletedAchievementIcon_secret;
    public Transform achievementWindow;
    public GameObject achievementIconPrefab;
    public Text tooltip;
    public string secretAchievementTooltipMessage;
    public GameObject popup;
    public float popupDuration;
    public Text popupName;
    public Image popupIcon;

    public Text completedAchievementNumber;
    public Text totalAchievementNumber;
    public Text resetAchievementText;

    private Queue<Achievement> achievementsToDisplay;
    private bool popupsDisplaying = false;
    private bool onlyScenarioAchievements = true;

    public void InitializeAchievements()
    {
        LoadAchievements();
        //Debug.Log("initialized " + achievementsd.Count + " achievements");
        Dictionary<string, List<int>> visibleAchievements = new Dictionary<string, List<int>>();
        Dictionary<string, List<int>> secretAchievements = new Dictionary<string, List<int>>();
        foreach (KeyValuePair<string, List<Achievement>> kvp in achievementsd)
        {
            //Debug.Log("Initialising achievements for " + kvp.Key);
            visibleAchievements.Add(kvp.Key, new List<int>());
            secretAchievements.Add(kvp.Key, new List<int>());
            List<Achievement> achievements = kvp.Value;
            for (int i = 0; i < achievements.Count; i++)
            {
                achievements[i].id = i;
                //Debug.Log("Achievement " + achievements[i].name + " has id " + achievements[i].id);

                if (achievements[i].secret)
                {
                    //Debug.Log("Adding achievement " + achievements[i].name + " from scenario " + kvp.Key + " to secret achievement list");
                    secretAchievements[kvp.Key].Add(i);
                }
                else
                {
                    //Debug.Log("Adding achievement " + achievements[i].name + " from scenario " + kvp.Key + " to visible achievement list");
                    visibleAchievements[kvp.Key].Add(i);
                }
            }
        }

        // Initialize visible achievement icons first, then secret achievement icons
        foreach (KeyValuePair<string, List<Achievement>> kvp in achievementsd)
        {

            foreach (int i in visibleAchievements[kvp.Key])
            {
                GameObject icon = Instantiate(achievementIconPrefab, achievementWindow);
                achievementsd[kvp.Key][i].uiIcon = icon;
            }
            foreach (int i in secretAchievements[kvp.Key])
            {
                GameObject icon = Instantiate(achievementIconPrefab, achievementWindow);
                achievementsd[kvp.Key][i].uiIcon = icon;
            }
        }
        achievementsToDisplay = new Queue<Achievement>();
    }

    private void LoadAchievements()
    {
        achievementsd = new Dictionary<string, List<Achievement>>();
        List<string> allScenarioNames = ScenarioManager.instance.allScenarioNames;
        foreach(string scenarioName in allScenarioNames)
        {
            List<Achievement> achievements = Resources.LoadAll(scenarioName, typeof(Achievement)).Cast<Achievement>().ToList();
            achievementsd.Add(scenarioName, achievements);
            //Debug.Log("loaded " + achievements.Count + " achievements for scenario " + scenarioName);
        }
    }

    public void DrawAchievementIcons()
    {
        int completedAchievements = 0;
        int totalAchievements = 0;
        foreach(KeyValuePair < string, List < Achievement >> kvp in achievementsd)
        {
            if (onlyScenarioAchievements && !kvp.Key.Equals(ScenarioManager.instance.GetCurrentScenarioName()))
            {
                //Debug.Log("Disabling all icons related to " + kvp.Key);
                DisableAllIcons(kvp.Value);
            }
            else
            {
                completedAchievements += DrawAchievementIcons(kvp.Value);
                totalAchievements += kvp.Value.Count;
            }
        }
        completedAchievementNumber.text = completedAchievements.ToString();
        totalAchievementNumber.text = totalAchievements.ToString();
        resetAchievementText.text = "Are you sure you want to reset all " + totalAchievements + " achievements?";
    }

    private void DisableAllIcons(List<Achievement> achievements)
    {
        foreach(Achievement achievement in achievements)
        {
            achievement.uiIcon.SetActive(false);
        }
    }

    private int DrawAchievementIcons(List<Achievement> achievements)
    {
        int completedAchievements = 0;
        for (int i = 0; i < achievements.Count; i++)
        {
            Achievement achievement = achievements[i];
            if (BoolPlayerPrefs.GetBool(achievement.name))
            {
                completedAchievements++;
                achievement.uiIcon.GetComponentInChildren<Image>().sprite = achievement.icon;
                achievement.uiIcon.SetActive(true);
            }
            else
            {
                if (achievement.secret)
                {
                    achievement.uiIcon.GetComponentInChildren<Image>().sprite = uncompletedAchievementIcon_secret;
                    achievement.uiIcon.SetActive(true);
                }
                else
                {
                    achievement.uiIcon.GetComponentInChildren<Image>().sprite = uncompletedAchievementIcon;
                    achievement.uiIcon.SetActive(true);
                }
            }
            UpdateAchievementTooltip(achievement);
        }
        return completedAchievements;
    }

    private void UpdateAchievementTooltip(Achievement achievement)
    {
        TooltipTrigger tooltipTrigger = achievement.uiIcon.GetComponent<TooltipTrigger>();
        tooltipTrigger.tooltipText = tooltip;
        if (achievement.secret && !BoolPlayerPrefs.GetBool(achievement.name)) // Hide secret && uncompleted achievement messages
        {
            tooltipTrigger.toolTipMessage = secretAchievementTooltipMessage;
        }
        else
        {
            tooltipTrigger.toolTipMessage = achievement.howToGet;
        }
    }

    public void TriggerAchievement(Achievement achievement)
    {
        if(!BoolPlayerPrefs.GetBool(achievement.name))
        {
            BoolPlayerPrefs.SetBool(achievement.name, true);
            achievementsToDisplay.Enqueue(achievement);
        
            if(!popupsDisplaying)
            {
                StartCoroutine(DisplayPopups());
            }
        }
    }

    private IEnumerator DisplayPopups()
    {
        popupsDisplaying = true;
        popup.SetActive(true);
        while(achievementsToDisplay.Count > 0)
        {
            Achievement achievement = achievementsToDisplay.Dequeue();
            //Debug.Log("Displaying popup for " + achievement.name + " at time " + Time.time);
            popupName.text = achievement.name;
            popupIcon.sprite = achievement.icon;
            yield return new WaitForSeconds(popupDuration);
        }
        popup.SetActive(false);
        popupsDisplaying = false;
    }

    public void SetOnlyScenarioAchievements(Toggle onlyScenarioAchievements)
    {
        //Debug.Log("Setting onlyScenarioAchievements to " + onlyScenarioAchievements.isOn);
        this.onlyScenarioAchievements = onlyScenarioAchievements.isOn;
        DrawAchievementIcons();
    }

    public void ResetAllAchievements()
    {
        foreach (KeyValuePair<string, List<Achievement>> kvp in achievementsd)
        {
            if (!onlyScenarioAchievements || kvp.Key.Equals(ScenarioManager.instance.GetCurrentScenarioName()))
            {
                Debug.Log("Resetting " + kvp.Value.Count + " achievements since onlyScenarioAchievements is " +onlyScenarioAchievements);
                foreach (Achievement achievement in kvp.Value)
                {
                    BoolPlayerPrefs.SetBool(achievement.name, false);
                }
            }
        }
    }
}
