﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System.IO;
using System;

public class ScenarioManager : MonoBehaviour {
    #region singleton
    public static ScenarioManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            throw new System.Exception("Found multiple instances of " + this.name);
        }
    }
    #endregion
    private List<Scenario> scenarios;
    public Button ScenarioPrefabButton;
    public Transform scenarioWindow;
    public Text tooltip;

    [HideInInspector]
    public List<String> allScenarioNames;

    private readonly string scenarioKey = "scenario_key";

    public void LoadAndInitilize()
    {
        scenarios = Resources.LoadAll("", typeof(Scenario)).Cast<Scenario>().ToList();
        //Debug.Log("Loaded " + scenarios.Count + " scenarios");
        InitializeScenarios();
        ChangeToScenario(GetCurrentScenario());
    }

    private Scenario GetCurrentScenario()
    {
        return GetScenarioByName(GetCurrentScenarioName());
    }

    public string GetCurrentScenarioName()
    {
        return PlayerPrefs.GetString(scenarioKey);
    }

    public int GetCurrentScenarioStatusIndex()
    {
        return GetCurrentScenario().statusLevelIndex;
    }

    private Scenario GetScenarioByName(string scenarioName)
    {
        foreach (Scenario scenario in scenarios)
        {
            if(scenario.name.Equals(scenarioName))
            {
                return scenario;
            }
        }
        Debug.LogError("Did not find a Scenario with name " + scenarioName);
        return null;
    }

    private void InitializeScenarios()
    {
        allScenarioNames = new List<string>();
        foreach (Scenario scenario in scenarios)
        {
            Directory.CreateDirectory("Assets/Resources/" + scenario.name + "/Achievements");
            Directory.CreateDirectory("Assets/Resources/" + scenario.name + "/Characters");
            Directory.CreateDirectory("Assets/Resources/" + scenario.name + "/DynamicStrings");
            Directory.CreateDirectory("Assets/Resources/" + scenario.name + "/StatusLevels");
            Directory.CreateDirectory("Assets/Resources/" + scenario.name + "/Locations");
            Directory.CreateDirectory("Assets/Resources/" + scenario.name + "/Zones");
            Directory.CreateDirectory("Assets/Resources/" + scenario.name + "/DialogueBackgrounds");
            Directory.CreateDirectory("Assets/Resources/" + scenario.name + "/Cards");
            Directory.CreateDirectory("Assets/Resources/" + scenario.name + "/AudioGroups");
            Button button = Instantiate(ScenarioPrefabButton, scenarioWindow);
            Text text = button.GetComponentInChildren<Text>();
            text.text = scenario.name;
            scenario.uiButton = button;
            UpdateScenarioTooltip(scenario);
            button.onClick.AddListener(() =>ChangeToScenario(scenario));
            allScenarioNames.Add(scenario.name);
        }
        //Debug.Log("Initialized " + allScenarioNames.Count + " scenarios");
    }

    private void UpdateScenarioTooltip(Scenario scenario)
    {
        TooltipTrigger tooltipTrigger = scenario.uiButton.GetComponent<TooltipTrigger>();
        tooltipTrigger.tooltipText = tooltip;
        tooltipTrigger.toolTipMessage = scenario.description;
    }

    public void ChangeToScenario(string scenarioName)
    {
        ChangeToScenario(GetScenarioByName(scenarioName));
    }
    
    public void ChangeToScenario(Scenario scenario)
    {
        if(scenario != null)
        {
            Debug.Log("Change to scenario " + scenario.name);
            GetCurrentScenario();
            PlayerPrefs.SetString(scenarioKey, scenario.name);
            CardLibrary.instance.SetStartingDeck(scenario);
            StartGameManager.instance.SetStartingDialogue(scenario);
            UIManager.instance.SetUIToScenario(scenario);
            MapNavigator.instance.UpdateMapAndLocationsToScenario(scenario);
            StatusManager.instance.ChangeToScenario(scenario);
            AudioManager.instance.SetDefaultMusicGroup(scenario.defaultMusic);
            StartGameManager.instance.ResetGameState();
        }
        else
        {
            Debug.Log("No current scenario");
        }
    }
}
