﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New SpriteGroup", menuName = "SpriteGroup")]
public class SpriteGroup : ScriptableObject
{
    public SpritePlacement spritePlacement;
    public List<Sprite> sprites;
    public List<AudioClip> audioClips;
    [HideInInspector]
    public int id;
}
public enum SpritePlacement {Right, Left}
