﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour {

    public Dialogue dialogue;

    public void TriggerDialogue()
    {
        //Debug.Log("Starting dialogue with " + dialogue.options.Count + " options");
        UIManager.instance.ChangeCursorToDefault();
        GenericDialogueManager.instance.PutDialogueOnQueue(dialogue);
    }
}
