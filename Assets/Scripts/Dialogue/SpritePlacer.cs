﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpritePlacer : MonoBehaviour
{
    public Image characterImageRight;
    public Image characterImageLeft;


	public Image GetImage(SpritePlacement spritePlacement)
	{
		Image characterImage = null;
		switch (spritePlacement)
		{
			case SpritePlacement.Right:
				characterImage = characterImageRight;
				break;
			case SpritePlacement.Left:
				characterImage = characterImageLeft;
				break;
			default:
				//should never happen
				break;
		}

		return characterImage;
	}

	internal void DeactivateSprite()
	{
		characterImageLeft.gameObject.SetActive(false);
		characterImageRight.gameObject.SetActive(false);
	}
}
