﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StHDialogueManager : GenericDialogueManager
{

	private bool waiting = false;
	private StatusLevel waitingStatus;
	private int waitingThreshold;

	public override void ResetDialogues()
	{
		base.ResetDialogues();
		ResetWaiting();
	}


	public override void StartDialogue(Dialogue dialogue)
	{
		base.StartDialogue(dialogue);
		
		// This makes the background images from the last dialogue stay during next dialogue, if the next dialogue doesn't have a background image.
		dialogueImage.gameObject.SetActive(true);
	}

	public override void NextDialogue()
	{
		//Debug.Log("Go to next dialogue, if any");
		EndDialogue();
		if (dialoguesToDisplay.Count > 0)
		{
			StartDialogue(dialoguesToDisplay.Dequeue());
		}
		else if (waiting)
		{
			TryToWait();

		}
	}
	private void TryToWait()
	{
		if (waitingStatus.currentLevel > waitingThreshold && waitingStatus.currentDelta < 0)
		{
			Debug.Log("Waiting for " + waitingStatus + " to reach " + waitingThreshold);
			while (currentlyDisplayingDialogue == null && waitingStatus.currentLevel > waitingThreshold)
			{
				StatusManager.instance.ChangeAllStatusesAtMove();
			}
		}
		else if (waitingStatus.currentLevel < waitingThreshold && waitingStatus.currentDelta > 0)
		{
			Debug.Log("Waiting for " + waitingStatus + " to reach " + waitingThreshold);
			while (currentlyDisplayingDialogue == null && waitingStatus.currentLevel < waitingThreshold)
			{
				StatusManager.instance.ChangeAllStatusesAtMove();
			}
		}
		else if (waitingStatus.currentLevel == waitingThreshold)
		{
			ResetWaiting();
		}
		else
		{
			Debug.LogError("Cannot wait for " + waitingStatus + " to reach " + waitingThreshold + " because current level is " + waitingStatus.currentLevel + " and delta is " + waitingStatus.currentDelta);
		}
	}

	public void WaitUntilStatusLevelReachesThreshold(StatusLevel statusLevel, int threshold)
	{
		if (!waiting)
		{
			Debug.Log("Waiting until status " + statusLevel + " reaches" + threshold);
			waiting = true;
			waitingStatus = statusLevel;
			waitingThreshold = threshold;
		}
		else
		{
			Debug.LogError("Cannot wait because we are already waiting (TODO: wait on multiple statuses/thresholds?)");
		}
	}

	private void ResetWaiting()
	{
		waiting = false;
		waitingStatus = null;
		waitingThreshold = 0;
	}
}
