﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
// using TMPro;

public class GenericDialogueManager : MonoBehaviour
{

	#region singleton
	public static GenericDialogueManager instance;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			throw new System.Exception("Found multiple instances of DialogueManager");
		}
	}
	#endregion

	public Text npcNameText; // This can be changed to TextMeshProUGUI
	public Text dialogueText; // This can be changed to TextMeshProUGUI
	public Image dialogueImage;
	public Transform dialogueBox;
	public Button continueButton;
	public Button optionButtonPrefab;
	public Button optionResultButton;
	public Button showDialogueButton;
	public Button hideDialogueButton;
	public SpritePlacer spritePlacer;

	public Animator animator;

	protected Queue<Dialogue> dialoguesToDisplay;
	private Queue<DialoguePart> parts;
	private List<DialogueOption> options;
	protected Dialogue currentlyDisplayingDialogue;

	private readonly string AND_OPERATOR = "&&";
	private readonly string OR_OPERATOR = "||";

	// Use this for initialization
	public virtual void ResetDialogues()
	{
		dialoguesToDisplay = new Queue<Dialogue>();
		parts = new Queue<DialoguePart>();
		options = new List<DialogueOption>();
		spritePlacer.DeactivateSprite();
		dialogueImage.gameObject.SetActive(false);
		hideDialogueButton.gameObject.SetActive(false);
		showDialogueButton.gameObject.SetActive(false);
		currentlyDisplayingDialogue = null;
	}

	public void PutDialogueOnQueue(Dialogue dialogue)
	{
		//Debug.Log("Putting dialogue " + dialogue.cardPath + " on queue");
		dialoguesToDisplay.Enqueue(dialogue);
		if (currentlyDisplayingDialogue == null)
		{
			StartDialogue(dialoguesToDisplay.Dequeue());
		}
	}

	public virtual void StartDialogue(Dialogue dialogue)
	{
		//Debug.Log("Starting dialogue");
		MoveDialogueBoxIn();
		PlayDialogueMusic(dialogue);
		hideDialogueButton.gameObject.SetActive(true);
		showDialogueButton.gameObject.SetActive(true);
		currentlyDisplayingDialogue = dialogue;
		DestroyOldOptionButtons();
		//Debug.Log("Dialogue has " + dialogue.options.Count + " options");
		parts.Clear();
		optionResultButton.gameObject.SetActive(false);
		options = dialogue.options;
		//Debug.Log("Has set " + options.Count + " options to be displayed at dialogue end. Dialogue has " + dialogue.options.Count);
		//Debug.Log("Dialogue consists of " + dialogue.parts.Length + " parts");
		foreach (DialoguePart part in dialogue.parts)
		{
			parts.Enqueue(part);
		}

		DisplayNextDialoguePart();
	}

	private void PlayDialogueMusic(Dialogue dialogue)
	{
		AudioManager.instance.PlayMusic(dialogue.musicGroup);
	}

	public void MoveDialogueBoxIn()
	{
		dialogueBox.gameObject.SetActive(true);
		animator.SetBool("IsOpen", true);
	}

	public void MoveDialogueBoxOut()
	{
		animator.SetBool("IsOpen", false);
	}

	private void DestroyOldOptionButtons()
	{
		OptionsButton[] buttons = dialogueBox.GetComponentsInChildren<OptionsButton>();
		//Debug.Log("Detroying " + buttons.Length + " option buttons");
		for (int i = 0; i < buttons.Length; i++)
		{
			GameObject.Destroy(buttons[i].gameObject);
		}
	}

	public void DisplayNextDialoguePart()
	{
		if (parts.Count == 0)
		{
			NextDialogue();
			//Debug.Log("NextDialogue called due to no parts");
			return;
		}
		if (parts.Count == 1)
		{
			DisplayOptionsIfAny();
		}
		else
		{
			continueButton.gameObject.SetActive(true);
		}
		DialoguePart part = parts.Dequeue();
		DisplayDialoguePart(part);

	}

	private void DisplayDialoguePart(DialoguePart part)
	{
		//Debug.Log("displaying dialogue part: " + part.sentence + " with spriteIndex " + part.spriteIndex);

		if (part.character != null)
		{
			//Debug.Log("Displaying character " + part.character.name);
			npcNameText.text = DynamicStringManager.instance.DecodeText(part.character.name);
			npcNameText.color = part.character.colour;
			spritePlacer.DeactivateSprite();
			if (!part.noSprite && part.character.GetSprite(part.spriteIndex) != null)
			{
				//Debug.Log("Displaying character sprite");
				SpritePlacement spritePlacement = part.character.GetSpritePlacement();
				Image characterImage = spritePlacer.GetImage(spritePlacement);
				Sprite sprite = part.character.GetSprite(part.spriteIndex);

				characterImage.sprite = sprite;
				characterImage.gameObject.SetActive(true);
			}
			else
			{
				//Debug.LogError("Character does not have a sprite");
			}
			PlayDialogueClipIfAny(part.character.GetAudioClip(part.spriteIndex));
		}
		else
		{
			npcNameText.text = "";
			spritePlacer.DeactivateSprite();
		}
		if (part.background != null)
		{
			dialogueImage.sprite = part.background;
			dialogueImage.gameObject.SetActive(true);
		}
		StopAllCoroutines();
		string decodedSentence = DynamicStringManager.instance.DecodeText(part.sentence);
		StartCoroutine(TypeSentence(decodedSentence));
	}


	private void PlayDialogueClipIfAny(AudioClip audioClip)
	{
		if (audioClip != null)
		{
			AudioManager.instance.PlayDialogueClip(audioClip);
		}
	}



	IEnumerator TypeSentence(string sentence)
	{
		dialogueText.text = "";
		foreach (char letter in sentence.ToCharArray())
		{
			dialogueText.text += letter;
			yield return null;
		}
	}

	private void DisplayOptionsIfAny()
	{
		//Debug.Log("Displaying " + options.Count + " options");
		if (options.Count > 0)
		{
			DisplayOptions();
			continueButton.gameObject.SetActive(false);
		}
		else
		{
			continueButton.gameObject.SetActive(true);
		}
	}

	private void DisplayOptions()
	{
		for (int i = 0; i < options.Count; i++)
		{
			if (RequirementsSatisfied(options[i]))
			{
				Button optionButton = Instantiate(optionButtonPrefab, dialogueBox);
				Text text = optionButton.gameObject.GetComponentsInChildren<Text>()[0];// This can be changed to TextMeshProUGUI
				if (text != null)
					text.text = DynamicStringManager.instance.DecodeText(options[i].buttonText);
				AddClickEventToOptionButton(optionButton, options[i]);
				//Debug.Log("Added optionButton " + options[i].buttonText);
			}
		}
	}

	private bool RequirementsSatisfied(DialogueOption dialogueOption)
	{
		switch (dialogueOption.requirementOperator)
		{
			case RequirementOperator.AND:
				bool allRequirementsSatisfied = true;
				for (int i = 0; i < dialogueOption.requirements.Count; i++)
				{
					DialogueOptionRequirement req = dialogueOption.requirements[i];
					StatusLevel statusLevel = req.statusLevel;
					allRequirementsSatisfied = allRequirementsSatisfied && statusLevel.StatusSatisfies(req.orderType, req.value);
				}

				return allRequirementsSatisfied;
			case RequirementOperator.OR:
				for (int i = 0; i < dialogueOption.requirements.Count; i++)
				{
					DialogueOptionRequirement req = dialogueOption.requirements[i];
					StatusLevel statusLevel = req.statusLevel;
					if (statusLevel.StatusSatisfies(req.orderType, req.value))
					{
						return true;
					}
				}
				return false;
			case RequirementOperator.CUSTOM:
				string reqRecipe = dialogueOption.customRequirementRecipe;
				bool requirementsSatisfied = EvaluateRequirements(dialogueOption, reqRecipe);
				Debug.Log("requirements satisfied for dialogue option " + dialogueOption.buttonText + ": " + requirementsSatisfied);
				return requirementsSatisfied;
			default:
				return false;
		}

	}

	private bool EvaluateRequirements(DialogueOption dialogueOption, string requirementRecipe)
	{
		Debug.Log("Evaluating : " + requirementRecipe);
		int startParIdx = requirementRecipe.IndexOf('(');
		//Debug.Log("startParIdx: " + startParIdx);
		if (startParIdx == -1)
		{
			//Debug.Log("Simple evaluation of: " + requirementRecipe);
			return EvaluateRequirementsNoParentheses(dialogueOption, requirementRecipe);
		}
		int endParIdx = FindEndParIdx(startParIdx, requirementRecipe);
		if (endParIdx == -1)
		{
			Debug.LogError("Invalid parenthesis at char no " + startParIdx + " in " + requirementRecipe);
			return false;
		}
		//Debug.Log("endParIdx: " + endParIdx);
		string middlePart = requirementRecipe.Substring(startParIdx + 1, endParIdx - startParIdx - 1);
		Debug.Log("middlePart: " + middlePart);
		bool requirementsSatisfied = EvaluateRequirements(dialogueOption, middlePart);
		if (startParIdx > 0)
		{
			string firstPart = requirementRecipe.Substring(0, startParIdx - 4);
			Debug.Log("firstPart: " + firstPart);
			string startOperator = requirementRecipe.Substring(startParIdx - 4, 4).Trim();
			Debug.Log("startOperator: " + startOperator);
			if (startOperator.Equals(AND_OPERATOR))
			{
				requirementsSatisfied = requirementsSatisfied && EvaluateRequirements(dialogueOption, firstPart);
			}
			else if (startOperator.Equals(OR_OPERATOR))
			{
				requirementsSatisfied = EvaluateRequirements(dialogueOption, firstPart) || requirementsSatisfied;
			}
			else
			{
				Debug.LogError("Invalid operator in requirementRecipe: " + startOperator + ", must be '" + AND_OPERATOR + "' or '" + OR_OPERATOR + "'.");
			}
		}
		if (endParIdx + 1 < requirementRecipe.Length)
		{
			string endOperator = requirementRecipe.Substring(endParIdx + 1, 4).Trim();
			Debug.Log("endOperator: " + endOperator);
			string lastPart = requirementRecipe.Substring(endParIdx + 5);
			Debug.Log("lastPart: " + lastPart);
			if (endOperator.Equals(AND_OPERATOR))
			{
				requirementsSatisfied = requirementsSatisfied && EvaluateRequirements(dialogueOption, lastPart);
			}
			else if (endOperator.Equals(OR_OPERATOR))
			{
				requirementsSatisfied = requirementsSatisfied || EvaluateRequirements(dialogueOption, lastPart);
			}
			else
			{
				Debug.LogError("Invalid operator in requirementRecipe: " + endOperator + ", must be '" + AND_OPERATOR + "' or '" + OR_OPERATOR + "'.");
			}
		}

		return requirementsSatisfied;
	}

	private bool EvaluateRequirementsNoParentheses(DialogueOption dialogueOption, string requirementRecipe)
	{
		string[] parts = requirementRecipe.Split(' ');
		bool requirementsSatisfied = true;
		for (int i = 0; i < parts.Length; i += 2)
		{
			int index;
			if (int.TryParse(parts[i], out index))
			{
				if (i == 0)
				{
					requirementsSatisfied = IsRequirementSatisfied(dialogueOption.requirements[index]);
				}
				else
				{
					string logicOperator = parts[i - 1];
					if (logicOperator.Equals(OR_OPERATOR))
					{
						requirementsSatisfied = requirementsSatisfied || IsRequirementSatisfied(dialogueOption.requirements[index]);
					}
					else if (logicOperator.Equals(AND_OPERATOR))
					{
						requirementsSatisfied = requirementsSatisfied && IsRequirementSatisfied(dialogueOption.requirements[index]);
					}
					else
					{
						Debug.LogError("Invalid operator in requirementRecipe: " + logicOperator + ", must be '" + OR_OPERATOR + "' or '" + AND_OPERATOR + "'.");
					}
				}
			}
		}
		Debug.Log("Evaluation of " + requirementRecipe + " returned " + requirementsSatisfied);
		return requirementsSatisfied;
	}

	private int FindEndParIdx(int startParIdx, string requirementRecipe)
	{
		int parLevel = 0;
		Char[] chars = requirementRecipe.ToCharArray();
		for (int i = startParIdx + 1; i < chars.Length; i++)
		{
			Char character = chars[i];
			//Debug.Log("Looking for ): at position " + i + ", found " + character);
			if (character.Equals('('))
			{
				parLevel++;
			}
			if (character.Equals(')'))
			{
				if (parLevel == 0)
				{
					return i;
				}
				parLevel--;
			}
		}
		return -1;
	}

	private bool IsRequirementSatisfied(DialogueOptionRequirement req)
	{
		StatusLevel statusLevel = req.statusLevel;
		Debug.Log("Checking if statuslevel " + statusLevel.name + " satisfies " + req.orderType + req.value);
		return statusLevel.StatusSatisfies(req.orderType, req.value);
	}

	private void AddClickEventToOptionButton(Button optionButton, DialogueOption dialogueOption)
	{
		optionResultButton.gameObject.SetActive(false);
		optionResultButton.onClick.RemoveAllListeners();
		optionResultButton.onClick.AddListener(() => NextDialogue());

		//Debug.Log("Adding " + dialogueOption.consequences.Count + " consequences to optionButton " + optionButton.name);
		foreach (DialogueConsequence consequence in dialogueOption.consequences)
		{
			optionButton.onClick.AddListener(() => EventManager.instance.HandleConsequence(consequence, optionResultButton));
		}
		if (dialogueOption.endingDialoguePart != null && dialogueOption.endingDialoguePart.sentence != null && !dialogueOption.endingDialoguePart.sentence.Equals(""))
		{
			//Debug.Log("sentence is " + dialogueOption.endingDialoguePart.sentence + " which is an empty string " + dialogueOption.endingDialoguePart.sentence.Equals(""));
			optionButton.onClick.AddListener(() => DisplayDialoguePart(dialogueOption.endingDialoguePart));
			optionButton.onClick.AddListener(DestroyOldOptionButtons);
			if (dialogueOption.goBackToOptions)
			{
				optionButton.onClick.AddListener(DisplayOptionsIfAny);
			}
			else
			{
				optionButton.onClick.AddListener(() => optionResultButton.gameObject.SetActive(true));
			}
		}
		else
		{
			optionButton.onClick.AddListener(NextDialogue);
			//optionButton.onClick.AddListener(() => Debug.Log("NextDialogue called from optionButton click"));
		}
	}

	public virtual void NextDialogue()
	{
		//Debug.Log("Go to next dialogue, if any");
		EndDialogue();
		if (dialoguesToDisplay.Count > 0)
		{
			StartDialogue(dialoguesToDisplay.Dequeue());
		}
	}


	public void EndDialogue()
	{
		MoveDialogueBoxOut();
		dialogueImage.gameObject.SetActive(false);
		spritePlacer.DeactivateSprite();
		hideDialogueButton.gameObject.SetActive(false);
		showDialogueButton.gameObject.SetActive(false);
		currentlyDisplayingDialogue = null;
	}

	public Dialogue getCurrentDialogue()
	{
		return currentlyDisplayingDialogue;
	}

	public Queue<Dialogue> getDialogueQueue()
	{
		return dialoguesToDisplay;
	}
}
