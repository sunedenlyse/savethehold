﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue
{
	public MusicGroup musicGroup;
	public DialoguePart[] parts;
	public List<DialogueOption> options;
	[HideInInspector]
	public int cardId;

}
[System.Serializable]
public class DialoguePart
{
	public Character character;
	[Range(0f, 10)]
	public int spriteIndex;
	[Tooltip("no sprite")]
	public bool noSprite;
	[TextArea(3, 10)]
	public string sentence;
	public Sprite background;
}

[System.Serializable]
public class DialogueOption
{
	public string buttonText;
	public List<DialogueOptionRequirement> requirements;
	public RequirementOperator requirementOperator;
	[Tooltip("e.g. 0 && (1 || 2)")]
	public string customRequirementRecipe;
	public List<DialogueConsequence> consequences;
	public DialoguePart endingDialoguePart;
	public bool goBackToOptions;
}

[System.Serializable]
public class DialogueOptionRequirement
{
	public StatusLevel statusLevel;
	public OrderType orderType;
	public int value;
}

public enum OrderType { Greater, GreaterOrEqual, Equal, LesserOrEqual, Lesser };

[System.Serializable]
public class DialogueConsequence
{
	public ConsequenceType consequenceType;
	public ConsequenceAddable addable;
	public int valueInt;
	public string valueString;
	public SpriteGroup valueSprite;
	public Location valueLocation;
}

public enum RequirementOperator { AND, OR, CUSTOM };

public enum ConsequenceType
{
	RemoveCard,
	AddCard,
	TriggerAchievement,
	EndGame,
	ChangeStatus,
	SetStatus,
	ChangeStatusDelta,
	SetStatusDelta,
	ImmediateDialogue,
	ChangeDynamicText,
	ChangeSprite,
	WaitUntilStatusReaches,
	ChangeReplaceActiveCard
};
