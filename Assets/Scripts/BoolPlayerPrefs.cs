﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoolPlayerPrefs : MonoBehaviour {

    #region singleton
    public static BoolPlayerPrefs instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            throw new System.Exception("Found multiple instances of " + this.name);
        }
    }
    #endregion

    public static void SetBool(string key, bool value)
    {
        if (value) {
            PlayerPrefs.SetInt(key, 1);
        }else
        {
            PlayerPrefs.SetInt(key, 0);
        }
    }

    public static bool GetBool(string key)
    {
        int value = PlayerPrefs.GetInt(key);
        if(value == 1)
        {
            return true;
        }
        else if (value == 0)
        {
            return false;
        }
        Debug.LogWarning("Did not find Playerprefs bool with key " + key + ", found value " + value);
        return false;
    }
}
