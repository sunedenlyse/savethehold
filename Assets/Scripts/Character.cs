﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Character", menuName = "Character")]
public class Character : ConsequenceAddable
{
    public new string name;
    public Color colour;
    public SpriteGroup originalSpriteGroup;
    private SpriteGroup spriteGroup;
    public List<string> expectedSprites;

    [HideInInspector]
    public int id;

    private readonly int defaultIndex = 0;

    public SpritePlacement GetSpritePlacement()
    {
        return spriteGroup.spritePlacement;
    }

    public Sprite GetSprite(int index)
    {
        //Debug.LogError("Getting Sprite at index " + index);
        if(spriteGroup == null)
        {
            Debug.LogError("Found no spritegroup for " + name);
        }
        if(spriteGroup.sprites.Count > index && spriteGroup.sprites[index] != null)
        {
            return spriteGroup.sprites[index];
        }
        else if(spriteGroup.sprites.Count > defaultIndex && spriteGroup.sprites[defaultIndex] != null)
        {
            Debug.LogError("Did not find a sprite for character " + name + " at index " + index);
            return spriteGroup.sprites[defaultIndex];
        }
        Debug.LogError("Did not find any sprite for character " + name);
        return null;
    }

    public AudioClip GetAudioClip(int index)
    {
        if(spriteGroup != null && spriteGroup.audioClips.Count > 0)
        {
            if (spriteGroup.audioClips.Count > index && spriteGroup.audioClips[index] != null)
            {
                return spriteGroup.audioClips[index];
            }
            else if (spriteGroup.audioClips[defaultIndex] != null)
            {
                Debug.Log("Did not find a audioClip for character " + name + " at index " + index);
                return spriteGroup.audioClips[defaultIndex];
            }
        }
        //Debug.Log("Did not find any audioClip for character " + name);
        return null;
    }

    public SpriteGroup GetSpriteGroup()
    {
        return spriteGroup;
    }

    public void SetSpriteGroup(SpriteGroup newSpriteGroup)
    {
        if (newSpriteGroup != null)
        {
            //Debug.Log("Changing spritegroup of " + this.name + " to " + newSpriteGroup.name);
            spriteGroup = newSpriteGroup;
        }
    }
}
