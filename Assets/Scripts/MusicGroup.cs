﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New MusicGroup", menuName = "MusicGroup")]
public class MusicGroup : ConsequenceAddable {

    public List<AudioClip> audioClips;
	
}
