﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitManager : MonoBehaviour {
    #region singleton
    public static ExitManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            throw new System.Exception("Found multiple instances of " + this.name);
        }
    }
    #endregion
    public GameObject gameEndPopUp;
    public GameObject confirmExitPopUp;

    private bool displayingConfirmExitPopup;
    private bool exitPopupAllowed = false;
    // Update is called once per frame
    void Update () {
        if(exitPopupAllowed)
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                if(displayingConfirmExitPopup)
                {
                    DisplayConfirmExitPopup(false);
                }
                else
                {
                    DisplayConfirmExitPopup(true);
                }
            }
        }
		
	}

    public void DisplayConfirmExitPopup(bool displayPopup)
    {
        confirmExitPopUp.SetActive(displayPopup);
        displayingConfirmExitPopup = displayPopup;
        if(displayPopup)
        {
            Time.timeScale = 0f;
        } else
        {
            Time.timeScale = 1f;
        }
    }

    public void ReturnToMenu()
    {
        gameEndPopUp.SetActive(false);
        DisplayConfirmExitPopup(false);
        exitPopupAllowed = false;
        UIManager.instance.SetMainMenuActive();
        StartGameManager.instance.ResetGameState();
    }

    public void ExitGame()
    {
        Debug.Log("Quit game");
        Application.Quit();
    }

    public void GameEndPopUp(MusicGroup endMusic)
    {
        if(endMusic != null)
        {
            AudioManager.instance.PlayMusic(endMusic);
        }
        gameEndPopUp.SetActive(true);
    }

    public void SetExitPopupAllowed(bool value)
    {
        exitPopupAllowed = value;
    }
}
