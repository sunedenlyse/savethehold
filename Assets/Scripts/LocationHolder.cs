﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocationHolder : MonoBehaviour {

    public Scenario scenarioToUseInEditor;
    public MapNavigator mapNavigator;

    public void ToggleLocationsInEditor(Toggle toggle)
    {
        mapNavigator.DestroyCurrentLocations();
        if(toggle.isOn)
        {
            mapNavigator.InstantiateLocations(scenarioToUseInEditor);
        }
    }

}
