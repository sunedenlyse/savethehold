﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class IdErrorCreator
{


    public static string GetErrorString(string assetType)
    {
        return "Found Multiple instances of " + assetType + " with same id. Run Ids/Instantiate Ids and rebuild";
    }
}
