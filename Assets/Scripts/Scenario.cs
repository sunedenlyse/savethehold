﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Scenario", menuName = "Scenario")]
public class Scenario : ScriptableObject {
    public string description;
    public List<Card> startingDeck;
    public Location startLocation;
    public Card startingDialogue;
    public MusicGroup defaultMusic;
    public MusicGroup menuMusic;
    public Sprite playerIcon;
    public Sprite explorationIcon;
    public Texture2D mapTexture;
    public UiSettings uiData;

    [HideInInspector]
    public Button uiButton;
    [HideInInspector]
    public int statusLevelIndex;
}

[System.Serializable]
public class UiSettings
{
    public Color popUpColor;
    public Sprite mainMenuBackground;
    public Sprite sideMenuBackground;
    public Color highlightedTextColor;
    public Font highlightedTextFont;
    public Font textFont;
    public Texture2D defaultCursor;
    public Vector2 defaultHotSpot;
    public Texture2D clickCursor;
    public Vector2 clickHotSpot;
}
