﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System;

public class SaveGameManager : MonoBehaviour
{
    #region singleton
    public static SaveGameManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            throw new System.Exception("Found multiple instances of " + this.name);
        }
    }
    #endregion

    public GameObject savedGameIconPrefab;
    public Transform savedGameWindow;
    public Text deleteSavedGameText;
    public GameObject confirmDeleteButton;
    public Text confirmLoadSavedGameText;
    public GameObject confirmLoadButton;
    public Text newOrOverwriteText;

    private string selectedSavedGameName;
    private Dictionary<string, GameObject> allSavedGameIcons;
    private readonly string savedGameExtension = ".sg";
    private string savedGamePath; // Has to be initialized in Start() or Awake().
    private ToggleGroup toggleGroup;
    private bool onlyScenarioSavedGames = true;

    public void SelectSavedGame(GameObject savedGameIcon)
    {
        selectedSavedGameName = savedGameIcon.GetComponentInChildren<Text>().text;
        UpdateDeleteSavedGameText();
        UpdateLoadSavedGameText();
    }

    public void DeselectSavedGame()
    {
        if(selectedSavedGameName != null)
        {
            if (allSavedGameIcons.ContainsKey(selectedSavedGameName)) {
                allSavedGameIcons[selectedSavedGameName].GetComponent<Toggle>().isOn = false;
            }
            selectedSavedGameName = null;
        }
        UpdateDeleteSavedGameText();
        UpdateLoadSavedGameText();
    }

    public void Initialize()
    {
        savedGamePath = Application.persistentDataPath;
        toggleGroup = savedGameWindow.GetComponent<ToggleGroup>();
        InitializeSavedGameIcons();
        UpdateDeleteSavedGameText();
        UpdateLoadSavedGameText();
    }

    private void InitializeSavedGameIcons()
    {
        allSavedGameIcons = new Dictionary<string, GameObject>();
        List<SavedGame> savedGames = GetAllSavedGameNames();
        foreach (SavedGame savedGame in savedGames)
        {
            InitializeSaveGameIcon(savedGame);
        }
    }

    public List<SavedGame> GetAllSavedGameNames()
    {
        List<SavedGame> savedGames = new List<SavedGame>();
        DirectoryInfo info = new DirectoryInfo(savedGamePath);
        FileInfo[] fileInfo = info.GetFiles();
        for (int i = 0; i < fileInfo.Length; i++)
        {
            if(savedGameExtension.Equals(fileInfo[i].Extension))
            {
                fileInfo[i].Name.Replace(savedGameExtension, "");
                SavedGame sg = LoadFromBinary(fileInfo[i].Name.Replace(savedGameExtension, ""));
                savedGames.Add(sg);
            }
        }
        return savedGames;
    }

    public void InitializeSaveGameIcon(SavedGame savedGame)
    {
        string savedGameName = savedGame.saveGameName;
        GameObject icon = Instantiate(savedGameIconPrefab, savedGameWindow);
        Text iconText = icon.GetComponentInChildren<Text>();
        iconText.text = savedGameName;
        UIManager.instance.SetTextFontAndColor(iconText);
        Toggle toggle = icon.GetComponent<Toggle>();
        toggle.onValueChanged.AddListener(delegate {
            if (toggle.isOn)
            {
                SelectSavedGame(icon);
            }
        });
        toggle.group = toggleGroup;
        //Debug.Log("Adding " + savedGameName + " to allSavedGameIcons");
        allSavedGameIcons.Add(savedGameName, icon);
        InitializeIconData(icon.GetComponent<SavedGameIconData>(), savedGame);
    }

    private void InitializeIconData(SavedGameIconData savedGameIconData, SavedGame savedGame)
    {
        savedGameIconData.scenarioName = savedGame.scenarioName;
    }

    public void DrawSavedGameIcons()
    {
        string currentScenarioName = ScenarioManager.instance.GetCurrentScenarioName();
        //Debug.Log("Drawing saved games icons - Only scenarioIcons is " + onlyScenarioSavedGames + ", and current scenario is " + currentScenarioName);
        foreach (KeyValuePair<string, GameObject> kvp in allSavedGameIcons)
        {
            GameObject icon = kvp.Value;
            string scenarioName = icon.GetComponent<SavedGameIconData>().scenarioName;
            if (onlyScenarioSavedGames && !scenarioName.Equals(currentScenarioName))
            {
                icon.SetActive(false);
                //Debug.Log("hiding saved game " + kvp.Key);
                if(selectedSavedGameName == kvp.Key)
                {
                    icon.GetComponent<Toggle>().isOn = false;
                    selectedSavedGameName = null;
                }
            }
            else
            {
                icon.SetActive(true);
            }
        }
    }

    public void UpdateLoadSavedGameText()
    {
        if (selectedSavedGameName != null)
        {
            confirmLoadSavedGameText.text = "Are you sure you want to load '" + selectedSavedGameName + "' saved game?";
            confirmLoadButton.SetActive(true);
        }
        else
        {
            confirmLoadSavedGameText.text = "No save game selected";
            confirmLoadButton.SetActive(false);
        }
    }

    public void UpdateDeleteSavedGameText()
    {
        if(selectedSavedGameName != null)
        {
            deleteSavedGameText.text = "Are you sure you want to delete '" + selectedSavedGameName + "' saved game?";
            confirmDeleteButton.SetActive(true);
        }
        else
        {
            deleteSavedGameText.text = "No save game selected";
            confirmDeleteButton.SetActive(false);
        }
    }

    public void UpdateNewOrOverwriteText(string saveGameName)
    {
        if (saveGameName == null || saveGameName.Trim().Equals(""))
        {
            newOrOverwriteText.text = "Saved game name must not be empty";
        }
        else
        {
            string savePath = savedGamePath + "/" + saveGameName + savedGameExtension;
            if(File.Exists(savePath)) {
                newOrOverwriteText.text = "Saving with this name will overwrite an existing save";
            }
            else
            {
                newOrOverwriteText.text = "Saving with this name will create a new Saved Game";
            }
        }
    }

    public void SetOnlyScenarioSavedGames(Toggle onlyScenarioSavedGames)
    {
        //Debug.Log("Setting only scenario saved games");
        this.onlyScenarioSavedGames = onlyScenarioSavedGames.isOn;
        DrawSavedGameIcons();
    }

    // *** DELETE *** //
    public void DeleteSelectedSavedGame()
    {
        if(selectedSavedGameName != null)
        {
            File.Delete(savedGamePath + "/" + selectedSavedGameName + savedGameExtension);
            Destroy(allSavedGameIcons[selectedSavedGameName]);
            allSavedGameIcons.Remove(selectedSavedGameName);
            DeselectSavedGame();
        }
    }

    // *** SAVING *** //
    #region saving
    public void SaveCurrentGame(Text saveGameName)
    {
        if(saveGameName.text != null && !saveGameName.text.Trim().Equals(""))
        {
            string savePath = savedGamePath + "/" + saveGameName.text + savedGameExtension;
            bool overwritingExistingFile = File.Exists(savePath);
            Debug.Log("Saving game at " + savePath + ", overwriting = " + overwritingExistingFile);
            SavedGame savedGame = CreateSavedGame(saveGameName);
            SaveToBinary(savePath, savedGame);
            if(!overwritingExistingFile)
            {
                InitializeSaveGameIcon(savedGame);
            }
        } else
        {
            Debug.LogWarning("Unable to save due to saveGameName = " + saveGameName.text);
        }
    }

    private SavedGame CreateSavedGame(Text saveGameName)
    {
        SavedGame savedGame = new SavedGame
        {
            saveGameName = saveGameName.text,
            scenarioName = ScenarioManager.instance.GetCurrentScenarioName()
        };
        // Save StatusLevels
        Dictionary<int, StatusLevel> statusLevels = StatusManager.instance.GetStatusLevels();
        savedGame.statusLevelIds = new int[statusLevels.Count];
        savedGame.statusLevelValues = new int[statusLevels.Count];
        savedGame.statusLevelDeltas = new int[statusLevels.Count];
        savedGame.statusLevelThresholdsTriggered = new bool[statusLevels.Count][];
        for (int i = 0; i < statusLevels.Count; i++)
        {
            //Debug.Log("Saving statusLevel " + (i + ScenarioManager.instance.GetCurrentScenarioStatusIndex()) + " at savegame index " + i);
            StatusLevel statusLevel = statusLevels[i+ ScenarioManager.instance.GetCurrentScenarioStatusIndex()]; 
            savedGame.statusLevelIds[i] = statusLevel.id;
            savedGame.statusLevelValues[i] = statusLevel.currentLevel;
            savedGame.statusLevelDeltas[i] = statusLevel.currentDelta;
            savedGame.statusLevelThresholdsTriggered[i] = new bool[statusLevel.consequenceThresholds.Count];
            for (int j = 0; j < statusLevel.consequenceThresholds.Count; j++)
            {
                savedGame.statusLevelThresholdsTriggered[i][j] = statusLevel.consequenceThresholds[j].alreadyTriggered;
            }
        }

        // Save dynamic strings
        Dictionary<string, string> dynamicStrings = DynamicStringManager.instance.dynamicStrings;
        savedGame.dynamicStringKeys = new string[dynamicStrings.Count];
        savedGame.dynamicStringValues = new string[dynamicStrings.Count];
        int k = 0;
        foreach (KeyValuePair<string, string> kvp in dynamicStrings)
        {
            savedGame.dynamicStringKeys[k] = kvp.Key;
            savedGame.dynamicStringValues[k] = kvp.Value;
            k++;
        }

        // Save character SpriteGroups
        List<Character> characters = CharacterManager.GetCharacters();
        savedGame.characterIds            = new int[characters.Count];
        savedGame.characterSpriteGroupIds = new int[characters.Count];
        for (int i = 0; i < characters.Count; i++)
        {
            Character character = characters[i];
            savedGame.characterIds[i]            = character.id;
            savedGame.characterSpriteGroupIds[i] = character.GetSpriteGroup().id;
        }

        // Save Dialogues
        Dialogue currentDialogue = StHDialogueManager.instance.getCurrentDialogue();
        Queue<Dialogue> queue = StHDialogueManager.instance.getDialogueQueue();
        if(currentDialogue != null)
        {
            int totalElmtsOnQueue = 1 + queue.Count;
            savedGame.currentlyDisplayingDialogueCardIds = new int[totalElmtsOnQueue];
            savedGame.currentlyDisplayingDialogueCardIds[0] = currentDialogue.cardId;
            //Debug.Log("Saving game with " + (totalElmtsOnQueue) + " Dialogues on queue");
            for (int i = 1; i < totalElmtsOnQueue; i++)
            {
                int cardId= queue.Dequeue().cardId;
                //Debug.Log("    Saving game with " + cardId + " on queue");
                savedGame.currentlyDisplayingDialogueCardIds[i] = cardId;
            }
        }

        // Save Deck
        List<Card> activeDeck = CardLibrary.instance.GetActiveDeck();
        List<Card> randomDeck = CardLibrary.instance.GetRandomDeck();
        savedGame.deckCardIds = new int[activeDeck.Count + randomDeck.Count];
        for (int i = 0; i < activeDeck.Count; i++)
        {
            //Debug.Log("Saving active Card " + i + " " + activeDeck[i].name);
            savedGame.deckCardIds[i] = activeDeck[i].dialogue.cardId;
        }
        for (int i = 0; i < randomDeck.Count; i++)
        {
            //Debug.Log("Saving random Card " + (i +activeDeck.Count) + " " + randomDeck[i].name);
            savedGame.deckCardIds[i+activeDeck.Count] = randomDeck[i].dialogue.cardId;
        }

        // Save replacesActiveCardsmap
        Location[] locations = MapNavigator.instance.locations.GetComponentsInChildren<Location>();
        List<Location> replacingLocations = new List<Location>();
        for (int i = 0; i < locations.Length; i++)
        {
            if (locations[i].replacesActiveCard != null)
            {
                replacingLocations.Add(locations[i]);
            }
        }
        savedGame.replacesActiveCard_LocationIds = new int[replacingLocations.Count];
        savedGame.replacesActiveCard_CardIds     = new int[replacingLocations.Count];
        for (int i = 0; i < replacingLocations.Count; i++)
        {
            Location location = replacingLocations[i];
            if (location.replacesActiveCard != null)
            {
                Debug.Log("Saving replacesActiveCard on location " + location.name + ", card " + location.replacesActiveCard.name + ", cardId " + location.replacesActiveCard.dialogue.cardId);
                savedGame.replacesActiveCard_LocationIds[i] = location.id;
                savedGame.replacesActiveCard_CardIds[i] = location.replacesActiveCard.dialogue.cardId;
            }
        }

        // Save current Location
        Location currentLocation = MapNavigator.instance.GetCurrentLocation();
        //Debug.Log("Save location " + currentLocation.name + " with id " + currentLocation.path);
        savedGame.currentLocationId = currentLocation.id;


        return savedGame;
    }

    private void SaveToBinary(string savePath, SavedGame savedGame)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        //Debug.Log("Saving to " + savePath);
        FileStream stream = new FileStream(savePath, FileMode.Create);
        formatter.Serialize(stream, savedGame);
        stream.Close();
    }

    #endregion

    // *** LOADING *** //
    #region loading
    public void LoadSelectedSaveGame()
    {
        if(selectedSavedGameName != null)
        {
            LoadSavedGame(selectedSavedGameName);
        }
    }

    public void LoadSavedGame(string saveGameName)
    {
        SavedGame savedGame = LoadFromBinary(saveGameName);
        if(savedGame != null)
        {

            ScenarioManager.instance.ChangeToScenario(savedGame.scenarioName);
            // Load StatusLevels
            StatusManager.instance.UpdateAllStatusLevels(savedGame.statusLevelIds, savedGame.statusLevelValues, savedGame.statusLevelDeltas, savedGame.statusLevelThresholdsTriggered);
                       
            // Load dynamic strings
            DynamicStringManager dynamicStringManager = DynamicStringManager.instance;
            for (int i = 0; i < savedGame.dynamicStringKeys.Length; i++)
            {
                dynamicStringManager.dynamicStrings[savedGame.dynamicStringKeys[i]] = savedGame.dynamicStringValues[i];
            }

            // Load character SpriteGroups
            CharacterManager.UpdateAllSpriteGroups(savedGame.characterIds, savedGame.characterSpriteGroupIds);

            // Load Dialogues
            //Debug.Log("Loaded game with " + savedGame.currentlyDisplayingDialogueCardPath.Length + " dialogues on queue");
            Dictionary<int, Card> cardsById = CardLibrary.GetAllCardsById();
            if (savedGame.currentlyDisplayingDialogueCardIds != null)
            {
                for (int i = 0; i < savedGame.currentlyDisplayingDialogueCardIds.Length; i++)
                {
                    int currentDialogueCardId = savedGame.currentlyDisplayingDialogueCardIds[i];
                    //Debug.Log("Loaded game has " + currentDialogueCardPath + " on queue");
                    Card card = cardsById[currentDialogueCardId];
                    GenericDialogueManager.instance.PutDialogueOnQueue(card.dialogue);
                }
            }

            // Load decks
            CardLibrary.instance.ResetDeck();
            int[] savedDeck = savedGame.deckCardIds;
            for (int i = 0; i < savedDeck.Length; i++)
            {
                Card card = cardsById[savedDeck[i]];
                //Debug.Log("Loading Card at " + savedDeck[i] + ", found " + card);
                CardLibrary.instance.AddCardToDeck(card);
            }

            // Load Replaces Active Cards
            for (int i = 0; i < savedGame.replacesActiveCard_CardIds.Length; i++)
            {
                Card card = cardsById[savedGame.replacesActiveCard_CardIds[i]];
                Debug.Log("Loading replacesActiveCard on location " + savedGame.replacesActiveCard_LocationIds[i] + ", card " + card.name + ", cardId " + card.dialogue.cardId);
                MapNavigator.instance.SetReplacesActiveCardOnLocation(savedGame.replacesActiveCard_LocationIds[i], card);
            }

            // Load Location
            //Debug.Log("Load location with id " + savedGame.currentLocationPath);
            Location loadedLocation = MapNavigator.instance.GetLocationById(savedGame.currentLocationId);
            MapNavigator.instance.MovePlayerToLocation(loadedLocation);


            StartGameManager.instance.StartGame();
            //Debug.Log("loaded " + savedGame.saveGameName + " with scenario " + savedGame.scenarioName);
        }
    }

    private SavedGame LoadFromBinary(string saveGameName)
    {
        string savePath = savedGamePath + "/" + saveGameName + ".sg";
        if (File.Exists(savePath))
        {
            Debug.Log("Loading file at " + savePath);
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(savePath, FileMode.Open);
            SavedGame savedGame = formatter.Deserialize(stream) as SavedGame;
            stream.Close();
            return savedGame;
        }
        else
        {
            Debug.LogError("Savefile not found in " + savePath);
            return null;
        }
    }
    #endregion
}

[System.Serializable]
public class SavedGame
{
    public string saveGameName;
    public string scenarioName;
    public int[] statusLevelIds;
    public int[] statusLevelValues;
    public int[] statusLevelDeltas;
    public bool[][] statusLevelThresholdsTriggered;
    public int[] currentlyDisplayingDialogueCardIds;
    public int[] deckCardIds;
    public int currentLocationId;
    public string[] dynamicStringKeys;
    public string[] dynamicStringValues;
    public int[] characterIds;
    public int[] characterSpriteGroupIds;
    public int[] replacesActiveCard_LocationIds;
    public int[] replacesActiveCard_CardIds;
}