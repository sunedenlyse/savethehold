﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New StatusLevel", menuName = "BoolStatusLevel")]
public class BoolStatusLevel : StatusLevel
{
    private readonly List<string> possibleEnumValues = new List<string>() {"False", "True"};

    public override bool HasPossibleValuesList()
    {
        return true;
    }

    public override List<string> GetPossibleValues()
    {
        return possibleEnumValues;
    }

}
