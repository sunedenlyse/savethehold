﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New StatusLevel", menuName = "StatusLevel")]
public class StatusLevel : ConsequenceAddable {

    public int startLevel;
    [HideInInspector]
    public int currentLevel;
    public int startDelta;
    [HideInInspector]
    public int currentDelta;
    public StatusVisibility visibility;
    public List<ConsequenceThreshold> consequenceThresholds;

    [HideInInspector]
    public StatusUI statusUI;
    [HideInInspector]
    public int id;


    public void ResetLevel () {
        currentLevel = startLevel;
        currentDelta = startDelta;
        for (int i = 0; i < consequenceThresholds.Count; i++)
        {
            ConsequenceThreshold threshold = consequenceThresholds[i];
            threshold.alreadyTriggered = false;
        }
    }
	
    public void ChangeBy(int delta)
    {
        bool visibleBefore = IsVisible();
        currentLevel += delta;
        //if (delta != 0) { Debug.Log("Changed status " + name + " by " + delta + ". Current level is " + currentLevel); }
        CheckThresholds();
        bool visibleAfter = IsVisible();
        if(visibleBefore != visibleAfter)
        {
            StatusManager.instance.HandleVisibilityChange(this);
        }
    }

    public void SetTo(int value, bool ignoreThresholds)
    {
        bool visibleBefore = IsVisible();
        currentLevel = value;
        //Debug.Log("Set status " + name + " to " + value);
        if(!ignoreThresholds)
        {
            CheckThresholds();
        }
        bool visibleAfter = IsVisible();
        if (visibleBefore != visibleAfter)
        {
            StatusManager.instance.HandleVisibilityChange(this);
        }
    }

    public void ChangeDeltaBy(int delta)
    {
        currentDelta += delta;
        //Debug.Log("Change status " + name + "delta by " + value);
    }

    public void SetDeltaTo(int value)
    {
        currentDelta = value;
    }

    private void CheckThresholds()
    {
        EventManager eventManager = EventManager.instance;
        for (int i = 0; i < consequenceThresholds.Count; i++)
        {
            ConsequenceThreshold threshold = consequenceThresholds[i];
            if (!threshold.alreadyTriggered && StatusSatisfies(threshold.orderType, threshold.value))
            {
                Debug.Log("Triggered consequence threshold " + name + " " + threshold.value);
                eventManager.HandleConsequence(threshold.consequence, null);
                threshold.alreadyTriggered = true;
            }
        }
    }

    public bool StatusSatisfies(OrderType orderType, int requiredValue)
    {
        switch (orderType)
        {
            case OrderType.Greater:
                return currentLevel > requiredValue;
            case OrderType.GreaterOrEqual:
                return currentLevel >= requiredValue;
            case OrderType.Equal:
                return currentLevel == requiredValue;
            case OrderType.LesserOrEqual:
                return currentLevel <= requiredValue;
            case OrderType.Lesser:
                return currentLevel < requiredValue;
            default:
                Debug.LogWarning("StatusLevel " + this.name + " could not be evaluated against order type " + orderType + " and value " + requiredValue);
                return false;
        }
    }

    public bool IsVisible()
    {
        return visibility.Equals(StatusVisibility.AlwaysVisible) || (visibility.Equals(StatusVisibility.VisibleWhenNeqZero) && currentLevel != 0);
    }

    public virtual bool HasPossibleValuesList()
    {
        return false;
    }

    public virtual List<string> GetPossibleValues()
    {
        return null;
    }
}

[System.Serializable]
public class ConsequenceThreshold
{
    public DialogueConsequence consequence;
    public OrderType orderType;
    public int value;
    [HideInInspector]
    public bool alreadyTriggered;
}

public enum StatusVisibility { Invisible, VisibleWhenNeqZero, AlwaysVisible}