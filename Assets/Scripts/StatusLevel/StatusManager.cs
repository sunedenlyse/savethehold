﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StatusManager : MonoBehaviour {
    #region singleton
public static StatusManager instance;
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            throw new System.Exception("Found multiple instances of StatusController");
        }
    }
    #endregion

    public GameObject showStatusButton;
    public Transform statusWindow;
    private Dictionary<int,StatusLevel> statusLevels;
    public GameObject StatusUIPrefab;

    private bool someStatusVisible;

    public Dictionary<int, StatusLevel> GetStatusLevels()
    {
        /*Debug.Log("Printing all status levels");
        foreach (KeyValuePair<int, StatusLevel> kvp in statusLevels)
        {

            StatusLevel statusLevel = kvp.Value;
            Debug.Log("Statuslevel with id " + kvp.Key + ": " + statusLevel.name + ", " + statusLevel.currentLevel);
        }*/
        return statusLevels;
    }

    public void ChangeToScenario(Scenario scenario)
    {
        ResetAllStatusLevels();
        DestroyAllStatusContents();
        List<StatusLevel> statusLevelsLoaded = Resources.LoadAll(scenario.name, typeof(StatusLevel)).Cast<StatusLevel>().ToList();
        statusLevels = new Dictionary<int, StatusLevel>();
        foreach (StatusLevel statusLevel in statusLevelsLoaded)
        {
            StatusUI statusUI = GameObject.Instantiate(StatusUIPrefab, statusWindow).GetComponent<StatusUI>();
            statusUI.statusName.text = statusLevel.name;
            statusLevel.statusUI = statusUI;
            if (!statusLevels.ContainsKey(statusLevel.id))
            {
                statusLevels.Add(statusLevel.id, statusLevel);
            }
            else
            {
                Debug.LogError(IdErrorCreator.GetErrorString("StatusLevel"));
            }
        }
    }

    public static int SetAllStatusIds()
    {
        int statusLevelCount = 0;
        List<Scenario> scenarios = Resources.LoadAll("", typeof(Scenario)).Cast<Scenario>().ToList();
        foreach(Scenario scenario in scenarios)
        {
            //Debug.Log("Settings ids for scenario " + scenario.name);
            scenario.statusLevelIndex = statusLevelCount;
            List<StatusLevel> statusLevels = Resources.LoadAll(scenario.name, typeof(StatusLevel)).Cast<StatusLevel>().ToList();
            for (int i = 0; i < statusLevels.Count; i++)
            {
                //Debug.Log("Settings id " + (i + scenario.statusLevelIndex) + " to " + statusLevels[i].name);
                statusLevels[i].id = i + scenario.statusLevelIndex;
                statusLevelCount++;
            }
            
        }
        return statusLevelCount;
    }

    public void UpdateAllStatusLevels(int[] ids, int[] values, int[] deltas, bool[][] statusLevelThresholdsTriggered)
    {
        for (int i = 0; i < ids.Length; i++)
        {
            UpdateStatusLevel(ids[i], values[i], deltas[i], statusLevelThresholdsTriggered[i]);
        }
        SetSomeStatusVisible(IsAnyStatusVisible());
    }

    private void UpdateStatusLevel(int id, int value, int delta, bool[] statusLevelThresholdsTriggered)
    {
        StatusLevel statusLevel = statusLevels[id];
        statusLevel.SetTo(value, true);
        statusLevel.SetDeltaTo(delta);
        for (int j = 0; j < statusLevel.consequenceThresholds.Count; j++)
        {
            statusLevel.consequenceThresholds[j].alreadyTriggered = statusLevelThresholdsTriggered[j];
        }
    }

    private void DestroyAllStatusContents()
    {
        StatusUI[] statusUIs = statusWindow.GetComponentsInChildren<StatusUI>();
        //Debug.Log("Detroying " + statusUIs.Length + " status UI icons in the status menu");
        foreach(StatusUI statusUI in statusUIs)
        {
            GameObject.Destroy(statusUI.gameObject);
        }
    }

    public void ResetAllStatusLevels() {
        if(statusLevels != null)
        {
            //Debug.Log("Resetting " + statusLevels.Count + " status levels");
            foreach(KeyValuePair<int, StatusLevel> kvp in statusLevels)
            {
                kvp.Value.ResetLevel();
            }
            SetSomeStatusVisible(IsAnyStatusVisible());
        }
    }

    public void HandleVisibilityChange(StatusLevel statusLevel)
    {
        if(statusLevel.IsVisible() && !someStatusVisible)
        {
            SetSomeStatusVisible(true);
        }
        if (!statusLevel.IsVisible())
        {
            SetSomeStatusVisible(IsAnyStatusVisible());
        }
    }

    private bool IsAnyStatusVisible()
    {
        foreach (KeyValuePair<int,StatusLevel> kvp in statusLevels)
        {
            if (kvp.Value.IsVisible())
            {
                return true;
            }
        }
        return false;
    }

    private void SetSomeStatusVisible(bool visible)
    {
        someStatusVisible = visible;
        showStatusButton.SetActive(visible);
    }

    public void ChangeAllStatusesAtMove()
    {
        foreach (KeyValuePair<int, StatusLevel> kvp in statusLevels)
        {
            StatusLevel statusLevel = kvp.Value;
            statusLevel.ChangeBy(statusLevel.currentDelta);
        }
    }

    public void ShowStatusMenu()
    {
        UIManager.instance.statusLevelUI.SetActive(true);
        UnDrawStatuses();
        DrawAllVisibleStatuses();
    }

    private void UnDrawStatuses()
    {
        Debug.Log("Remove old statuses");
    }

    private void DrawAllVisibleStatuses()
    {
        foreach (KeyValuePair<int, StatusLevel> kvp in statusLevels)
        {
            StatusLevel statusLevel = kvp.Value;
            if (statusLevel.IsVisible())
            {
                statusLevel.statusUI.statusLevel.text = statusLevel.currentLevel.ToString();
                statusLevel.statusUI.gameObject.SetActive(true);
            } else
            {
                statusLevel.statusUI.gameObject.SetActive(false);
            }
        }
    }

}
