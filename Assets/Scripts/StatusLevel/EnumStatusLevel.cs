﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New StatusLevel", menuName = "EnumStatusLevel")]
public class EnumStatusLevel : StatusLevel
{
    public List<string> possibleEnumValues;

    public override bool HasPossibleValuesList()
    {
        return true;
    }

    public override List<string> GetPossibleValues()
    {
        return possibleEnumValues;
    }

}
