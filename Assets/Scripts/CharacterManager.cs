﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CharacterManager : MonoBehaviour {
    #region singleton
    public static CharacterManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            throw new System.Exception("Found multiple instances of CharacterManager");
        }
    }
    #endregion

    public void ResetCharacterSprites()
    {
        List<Character> characters = GetCharacters();
        foreach(Character character in characters)
        {
            character.SetSpriteGroup(character.originalSpriteGroup);
        }
    }

    public static void UpdateAllSpriteGroups(int[] characterIds, int[] spritegroupIds)
    {
        Dictionary<int, Character> charactersById = GetCharactersById();
        Dictionary<int, SpriteGroup> spriteGroupsById = GetSpriteGroupsById();

        for (int i = 0; i < characterIds.Length; i++)
        {
            Character character = charactersById[characterIds[i]];
            SpriteGroup spriteGroup = spriteGroupsById[spritegroupIds[i]];
            character.SetSpriteGroup(spriteGroup);
        }
    }

    public static List<Character> GetCharacters()
    {
        return Resources.LoadAll("", typeof(Character)).Cast<Character>().ToList();
    }

    private static Dictionary<int, Character> GetCharactersById()
    {
        List<Character> characters = GetCharacters();
        Dictionary<int, Character> charactersById = new Dictionary<int, Character>();
        foreach (Character character in characters)
        {
            if (!charactersById.ContainsKey(character.id))
            {
                charactersById.Add(character.id, character);
            }
            else
            {
                Debug.LogError(IdErrorCreator.GetErrorString("Character"));
            }
        }

        return charactersById;
    }

    public static List<SpriteGroup> GetSpriteGroups()
    {
        return Resources.LoadAll("", typeof(SpriteGroup)).Cast<SpriteGroup>().ToList();
    }

    private static Dictionary<int, SpriteGroup> GetSpriteGroupsById()
    {
        List<SpriteGroup> spriteGroups = GetSpriteGroups();
        Dictionary<int, SpriteGroup> spriteGroupsById = new Dictionary<int, SpriteGroup>();
        foreach (SpriteGroup spriteGroup in spriteGroups)
        {
            if (!spriteGroupsById.ContainsKey(spriteGroup.id))
            {
                spriteGroupsById.Add(spriteGroup.id, spriteGroup);
            }
            else
            {
                Debug.LogError(IdErrorCreator.GetErrorString("SpriteGroup"));
            }
        }

        return spriteGroupsById;
    }
}
