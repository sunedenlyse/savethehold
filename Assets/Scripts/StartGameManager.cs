﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameManager : MonoBehaviour {
    #region singleton
    public static StartGameManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            throw new System.Exception("Found multiple instances of " + this.name);
        }
    }
    #endregion
    public MusicGroup mainMenuMusic;

    private Card startingDialogue;

    private void Start()
    {
        UIManager.instance.SetMainMenuActive();
        UIManager.instance.InitializeOptionsUI();
        ScenarioManager.instance.LoadAndInitilize();
        AchievementManager.instance.InitializeAchievements();
        SaveGameManager.instance.Initialize();
    }

    public void ResetGameState()
    {
        Debug.Log("Resetting game state");
        CardLibrary.instance.SetDeckToStartingDeck();
        StatusManager.instance.ResetAllStatusLevels();
        GenericDialogueManager.instance.ResetDialogues();
        DynamicStringManager.instance.ResetDynamicStrings();
        CharacterManager.instance.ResetCharacterSprites();
        MapNavigator.instance.ResetAllReplacesActiveCardOnLocation();
        MapNavigator.instance.ResetPlayerPosition();
    }

    public void SetStartingDialogue(Scenario scenario)
    {
        startingDialogue = scenario.startingDialogue;
    }

    public void StartNewGame()
    {
        Debug.Log("Starting new game");
        StartGame();
        GenericDialogueManager.instance.PutDialogueOnQueue(startingDialogue.dialogue);
    }

    public void StartGame()
    {
        UIManager.instance.SetAllMenusInactive();
        ExitManager.instance.SetExitPopupAllowed(true);
    }
}
