﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New DynamicString", menuName = "DynamicString")]
public class DynamicString : ConsequenceAddable {
    public string key;
	public string defaultValue;
}
