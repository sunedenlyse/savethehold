﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIManager : MonoBehaviour {


    #region singleton
    public static UIManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            throw new System.Exception("Found multiple instances of " + this.name);
        }
    }
    #endregion

    public GameObject mainMenuUI;
    public GameObject loadGameUI;
    public GameObject achievementUI;
    public GameObject scenarioUI;
    public GameObject optionsUI;
    public GameObject statusLevelUI;
    public Canvas canvas;
    public List<Image> sideMenuBackgrounds;
    public List<Image> popups;
    //Main menu UI
    public Text mainMenuHeader;
    public Image mainMenuBackground;
    // Status UI
    public StatusUI statusUIPrefab;
    // Achievement UI
    public Image achievementFractionLine;
    // Options UI
    public Toggle musicToggle;
    public Toggle dialogueToggle;

    private MusicGroup menuMusic;
    private Font font;
    private Font highlightedFont;
    private Color highlightedColor;

    // Cursor
    private Texture2D defaultCursor;
    private Texture2D clickCursor;
    private Vector2 defaultHotSpot;
    private Vector2 clickHotSpot;

    public void InitializeOptionsUI()
    {
        dialogueToggle.isOn = !BoolPlayerPrefs.GetBool(AudioManager.muteDialogueKey);
        musicToggle.isOn = !BoolPlayerPrefs.GetBool(AudioManager.muteMusicKey);
    }

    public void SetMainMenuActive()
    {
        loadGameUI.SetActive(false);
        mainMenuUI.SetActive(true);
        achievementUI.SetActive(false);
        scenarioUI.SetActive(false);
        statusLevelUI.SetActive(false);
        optionsUI.SetActive(false);
        PlayMainMenuTheme();
    }

    public void SetLoadGameUIActive()
    {
        mainMenuUI.SetActive(false);
        loadGameUI.SetActive(true);
        achievementUI.SetActive(false);
        scenarioUI.SetActive(false);
        statusLevelUI.SetActive(false);
        optionsUI.SetActive(false);
    }

    public void SetAchievementUIActive()
    {
        mainMenuUI.SetActive(false);
        loadGameUI.SetActive(false);
        achievementUI.SetActive(true);
        scenarioUI.SetActive(false);
        statusLevelUI.SetActive(false);
        optionsUI.SetActive(false);
    }

    public void SetScenarioUIActive()
    {
        mainMenuUI.SetActive(false);
        loadGameUI.SetActive(false);
        achievementUI.SetActive(false);
        scenarioUI.SetActive(true);
        statusLevelUI.SetActive(false);
        optionsUI.SetActive(false);
    }

    public void SetOptionsUIActive()
    {
        mainMenuUI.SetActive(false);
        loadGameUI.SetActive(false);
        achievementUI.SetActive(false);
        scenarioUI.SetActive(false);
        statusLevelUI.SetActive(false);
        loadGameUI.SetActive(false);
        optionsUI.SetActive(true);
    }

    public void SetAllMenusInactive()
    {
        mainMenuUI.SetActive(false);
        loadGameUI.SetActive(false);
        achievementUI.SetActive(false);
        scenarioUI.SetActive(false);
        statusLevelUI.SetActive(false);
        optionsUI.SetActive(false);
    }

    public void PlayMainMenuTheme()
    {
        AudioManager.instance.PlayMusic(menuMusic);
    }

    public void SetUIToScenario(Scenario scenario)
    {
        menuMusic = scenario.menuMusic;
        mainMenuHeader.text = scenario.name;
        mainMenuBackground.sprite = scenario.uiData.mainMenuBackground;
        SetSideMenuBackgrounds(scenario);
        SetPopUpColor(scenario);
        SetTexts(scenario);
        SetCursors(scenario);
        PlayMainMenuTheme();
    }

    private void SetCursors(Scenario scenario)
    {
        clickCursor = scenario.uiData.clickCursor;
        clickHotSpot = scenario.uiData.clickHotSpot;
        defaultCursor = scenario.uiData.defaultCursor;
        defaultHotSpot = scenario.uiData.defaultHotSpot;
    }

    private void SetTexts(Scenario scenario)
    {
        font = scenario.uiData.textFont;
        highlightedFont = scenario.uiData.highlightedTextFont;
        highlightedColor = scenario.uiData.highlightedTextColor;
        Text[] texts = canvas.GetComponentsInChildren<Text>(true);
        foreach (Text text in texts)
        {
            SetTextFontAndColor(text);
        }
        SetTextFontAndColor(StHDialogueManager.instance.optionButtonPrefab.GetComponentInChildren<Text>());
        SetTextFontAndColor(statusUIPrefab.statusLevel);
        SetTextFontAndColor(statusUIPrefab.statusName);
        achievementFractionLine.color = highlightedColor;
    }

    public void SetTextFontAndColor(Text text)
    {
        if (text is HighlightedText)
        {
            text.font = highlightedFont;
            text.color = highlightedColor;
        }
        else
        {
            text.font = font;
        }
    }

    private void SetSideMenuBackgrounds(Scenario scenario)
    {
        foreach (Image sideMenuBackground in sideMenuBackgrounds)
        {
            sideMenuBackground.sprite = scenario.uiData.sideMenuBackground;
        }
    }

    private void SetPopUpColor(Scenario scenario)
    {
        foreach(Image popupImage in popups)
        {
            popupImage.color = scenario.uiData.popUpColor;
        }
    }

    public void ChangeCursorToClick()
    {
        //Debug.Log("Change cursor to " + clickCursor.name + " with hotspot " + clickHotSpot.x + ", " + clickHotSpot.y);
        Cursor.SetCursor(clickCursor, clickHotSpot, CursorMode.Auto);
    }

    public void ChangeCursorToDefault()
    {
        Cursor.SetCursor(defaultCursor, defaultHotSpot, CursorMode.Auto);
    }
}
