﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System;

public class CardLibrary : MonoBehaviour {

    #region singleton
    public static CardLibrary instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            throw new System.Exception("Found multiple instances of CardLibrary");
        }
    }
    #endregion

    public Card defaultCard;
    
    private List<Card> startingDeck;
    private List<Card> randomCardDeck;
    private List<Card> activeCardDeck;

   

    public List<Card> GetRandomDeck()
    {
        return randomCardDeck;
    }

    public List<Card> GetActiveDeck()
    {
        return activeCardDeck;
    }
    public void SetStartingDeck(Scenario scenario)
    {
        this.startingDeck = scenario.startingDeck;
    }

    public void SetDeckToStartingDeck()
    {
        ResetDeck();
        for (int i = 0; i < startingDeck.Count; i++)
        {
            AddCardToDeck(startingDeck[i]);
        }
    }

    public void ResetDeck()
    {
        randomCardDeck = new List<Card>();
        activeCardDeck = new List<Card>();
    }

    /*
    public void ResetReplacesActiveDialogueCards()
    {
        List<Location> loadedLocations = Resources.LoadAll("", typeof(Location)).Cast<Location>().ToList();
        foreach(Location location in loadedLocations)
        {
            if(location.replacesActiveDialogue != null)
            {
                location.currentReplacesActiveDialogue = location.replacesActiveDialogue;
                Debug.Log("set current replaces active card on " + location.name + " to " + location.currentReplacesActiveDialogue.name);
            }
        }
    }
    */

    public Card DrawRandomCardFromDeck(Location location)
    {
        List<Card> questCards = new List<Card>();
        List<Card> normalCards = new List<Card>();
        for (int i = 0; i < randomCardDeck.Count; i++)
        {
            //Debug.Log("i is " + i + ", deck count is " + randomCardDeck.Count);
            Card card = randomCardDeck[i];
            if(IsCardEligibleInZones(card, location))
            {
                AddCardToDrawList(questCards, normalCards, card);
            }
        }
        //Debug.Log("Added all cards to lists (" + questCards.Count  + " questCards, " + normalCards.Count + " normalCards), now pick a random one of higest priority");
        if(questCards.Count > 0)
        {
            int index = UnityEngine.Random.Range(0, questCards.Count);
            Card randomQuestCard = questCards[index];
            RemoveSingleCardFromDeck(randomQuestCard);
            //Debug.Log("Drew a random card: " + randomQuestCard.name);
            return randomQuestCard;
        }
        if (normalCards.Count > 0)
        {
            int index = UnityEngine.Random.Range(0, normalCards.Count);
            Card randomNormalCard = normalCards[index];
            RemoveSingleCardFromDeck(randomNormalCard);
            //Debug.Log("Drew a random card: " + randomNormalCard.name + " at " + index);
            return randomNormalCard;
        }
        return defaultCard;
    }

    public List<Card> GetActiveCards(Location location)
    {
        List<Card> cards = new List<Card>();
        for (int i = 0; i < activeCardDeck.Count; i++)
        {
            if (IsCardEligibleInZones(activeCardDeck[i], location))
            {
                cards.Add(activeCardDeck[i]);
            }
        }
        //Debug.Log("Getting " + cards.Count +" Active Cards");
        return cards;
    }

    private bool IsCardEligibleInZones(Card card, Location location)
    {
        if (card.allowedZones.Count == 0 && card.allowedLocations.Count == 0)
        {
            //Debug.Log("Card " + card.name + " has no zone restrictions");
            return true; //no allowed zones or Locations means that the card is not restricted to any specific zones.
        }
        else
        {
            foreach(Location allowedLocation in card.allowedLocations)
            {
                //Debug.Log("card " + card.name + " has allowed location " + allowedLocation.name);
                if (allowedLocation.name.Equals(location.name))
                {
                    return true;
                }
            }
            for (int j = 0; j < card.allowedZones.Count; j++)
            {
                Zone allowedZone = card.allowedZones[j];
                //Debug.Log("Checking allowed locations for " + card.name);// + " in zone" + allowedZone.name);
                foreach (Location allowedLocation in allowedZone.locations)
                {
                    //Debug.Log("card " + card.name + " has allowed location " + allowedLocation.name);
                    if (allowedLocation.name.Equals(location.name))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void AddCardToDeck(Card card)
    {
        if(card.cardType.Equals(CardType.Random) || card.cardType.Equals(CardType.randomAndActive))
        {
            //Debug.Log("Adding " + card.name + " to random deck");
            randomCardDeck.Add(card);
        }
        if (card.cardType.Equals(CardType.Active) || card.cardType.Equals(CardType.randomAndActive))
        {
            //Debug.Log("Adding " + card.name + " to active deck");
            activeCardDeck.Add(card);
        }
    }
    
    public void RemoveSingleCardFromDeck(Card card)
    {
        randomCardDeck.Remove(card);
        activeCardDeck.Remove(card);
    }

    private static void AddCardToDrawList(List<Card> questCards, List<Card> normalCards, Card card)
    {
        if (card.priority.Equals(CardPriority.quest))
        {
            questCards.Add(card);
        }
        else if (card.priority.Equals(CardPriority.normal))
        {
            normalCards.Add(card);
        }
    }

    internal static Dictionary<int, Card> GetAllCardsById()
    {
        Dictionary<int, Card> cardsById = new Dictionary<int, Card>();
        UnityEngine.Object[] allCards = Resources.LoadAll("", typeof(Card));
        for (int i = 0; i < allCards.Length; i++)
        {
            Card card = allCards[i] as Card;
            cardsById.Add(card.dialogue.cardId, card);
        }
        return cardsById;
    }
}
