﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DynamicStringManager : MonoBehaviour {

    #region singleton
    public static DynamicStringManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            throw new System.Exception("Found multiple instances of " + this.name);
        }
    }
    #endregion

    public Dictionary<string, string> dynamicStrings;
    List<DynamicString> originalDynamicStrings;

    public void ResetDynamicStrings()
    {
        if(originalDynamicStrings == null)
        {
            originalDynamicStrings = Resources.LoadAll("", typeof(DynamicString)).Cast<DynamicString>().ToList();
        }
        //Debug.Log("Resetting Dynamic Strings");
        dynamicStrings = new Dictionary<string, string>();
        for (int i = 0; i < originalDynamicStrings.Count; i++)
        {
            dynamicStrings.Add(originalDynamicStrings[i].key, originalDynamicStrings[i].defaultValue);
        }
        //Debug.Log("Resetting Dynamic Strings: Found " + dynamicStrings.Keys.Count + " dynamic strings");
    }

    public void ChangeDynamicString(DynamicString dynamicString, string newValue)
    {
        dynamicStrings[dynamicString.key] = newValue;
    }

    public string DecodeText(string text)
    {
        string decodedText = text;
        foreach (KeyValuePair<string, string> dynamicDialogueString in dynamicStrings)
        {
            decodedText = decodedText.Replace(dynamicDialogueString.Key, dynamicDialogueString.Value);
        }
        return decodedText;
    }
}
