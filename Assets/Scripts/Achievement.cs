﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Achievement", menuName = "Achievement")]
public class Achievement : ConsequenceAddable
{
    public new string name;
    public string howToGet;
    public bool secret;
    public Sprite icon;

    [HideInInspector]
    public GameObject uiIcon;
    [HideInInspector]
    public int id;
}

